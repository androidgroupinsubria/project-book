- ProjectBook

Projectbook è un applicazione per il s.o. Android , sviluppata con il linguaggio Kotlin in team.
Projectbook è un social network che permette di pubblicare i propri progetti , idee, ecc...
Si ha la possibilità di contattare gli altri utenti tramite messaggistica istantanea con notifiche a comparsa.
Si ha la possibilità di creare gruppi di utenti.
Si possono eliminare i progetti ormai conclusi.
Personalizzare il proprio profilo inserendo informazioni personali e cambiando immagine profilo.
Si possono aggiungere e togliere membri ai gruppi già creati da parte del creatore del gruppo.
Si può eiminare un gruppo.
I dati utente verranno salvati in un DB locale.
Si possono creare / visualizzare / eliminare i propri progetti.
Nella homepage si ha accesso agli ultimi progetti inseriti dagli utenti.
Si può chiedere di partecipare ad un progetto.

- Sviluppato con

Android Studio

- Librerie esterne utilizzate

Picasso

- Autori

Andrea Peluso - andreapeluso97@gmail.com
Luca Cremonesi - luca.cremo99@gmail.com
Gabriele De Fino - gdefino@studenti.uninsubria.it





Il progetto è distribuito sotto la licenza MIT.
Copyright (c) 2020

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.