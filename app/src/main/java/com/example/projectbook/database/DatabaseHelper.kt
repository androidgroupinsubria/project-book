package com.example.projectbook.database

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import kotlin.collections.HashMap


class DatabaseHelper(context: Context) :
    SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    companion object {
        const val TAG = "DatabaseHelper"
        // If you change the database schema, you must increment the database version.
        const val DATABASE_VERSION = 2
        const val DATABASE_NAME = "user.db"
        // Books table name
        const val TABLE_NAME = "users"
        // Books Table Columns names
        const val UID = "uid"
        const val EMAIL = "email"
        const val USERNAME = "username"
        const val PHOTO = "photo"


        private const val SQL_CREATE_ENTRIES =
            "CREATE TABLE $TABLE_NAME (" +
                    "$UID text primary key, " +
                    "$EMAIL text not null," +
                    "$USERNAME text not null," +
                    "$PHOTO text);"

        private const val SQL_SELECT_user = "SELECT $USERNAME,$PHOTO FROM $TABLE_NAME WHERE $UID=?"

        private const val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS $TABLE_NAME"

    }


    @Override
    override fun onCreate(db: SQLiteDatabase) {
        // create new table
        db.execSQL(SQL_CREATE_ENTRIES)
        Log.d(TAG, "onCreate(): create table")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES)
        onCreate(db)
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }

    //metodo per recuperare le informazioni dell'utente dal database
    @SuppressLint("Recycle")
    fun getUserSettings(uid: String): HashMap<String, String> {
        val db = this.writableDatabase
        //salvo il risultato in un HashMap
        val map: HashMap<String, String> = hashMapOf("" to "")
        var email = ""
        var photo = ""
        val cursor: Cursor = db.rawQuery(SQL_SELECT_user, arrayOf(uid))
        if (cursor.moveToFirst()) {
            do {
                email = cursor.getString(0)
                photo = cursor.getString(1)
            } while (cursor.moveToNext())
        }
        db.close()
        map[email] = photo
        return map
    }


    //metodo per inserire un nuovo utente
    fun insertUserSettings(uid: String, email: String, username: String, photo: String): Long {
        val result: Long
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(UID, uid)
        values.put(EMAIL, email)
        values.put(USERNAME, username)
        values.put(PHOTO, photo)
        result = db.insert(TABLE_NAME, null, values)
        db.close()
        return result
    }

    //metodo per aggiornare i dati utente
    fun updateUserSettings(uid: String, email: String, username: String, photo: String): Int {
        val result: Int
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(EMAIL, email)
        values.put(USERNAME, username)
        values.put(PHOTO, photo)
        result = db.update(TABLE_NAME, values, "$UID='$uid'", null)
        db.close()
        return result
    }
}