package com.example.projectbook.groups

import android.annotation.SuppressLint
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.projectbook.R
import com.example.projectbook.models.ChatMessage
import com.example.projectbook.models.Group
import com.example.projectbook.models.User
import com.example.projectbook.views.SelectedUserItem
import com.example.projectbook.views.UserItemGroup
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.activity_add_group_users.*
import java.lang.Exception

class AddGroupUsers : AppCompatActivity() {
    companion object UserData {
        var selectedUsers: ArrayList<User> = ArrayList()
        var listUsers: ArrayList<User> = ArrayList()
        var membersList: ArrayList<String> = ArrayList()

        private val adapter = GroupAdapter<GroupieViewHolder>()
        private val adapter_add = GroupAdapter<GroupieViewHolder>()

        private var mContext: Context? = null
        fun getContext(): Context? {
            return mContext
        }

        fun addItem(item: Item<GroupieViewHolder>, recyclerview: RecyclerView) {
            try{
                adapter.remove(item)
                val userItem = item as UserItemGroup
                adapter_add.add(0, SelectedUserItem(userItem.user, recyclerview, false))
            }catch (e: Exception){}

        }

        fun addUserToList(user:User){
            selectedUsers.add(user)
        }

        fun removeUserToList(user:User){
            selectedUsers.remove(user)
        }

        fun removeItem(item: Item<GroupieViewHolder>, recyclerview: RecyclerView) {
            try {
                adapter_add.remove(item)
                refreshUsersList(recyclerview)
            }catch (e: Exception){}

        }

        fun getAdapterCount(): Int {
            return adapter_add.itemCount
        }

        fun refreshUsersList(recyclerview: RecyclerView){
            //pulisci l'adapter
            adapter.clear()
            //aggiungo gli oggetti all'adapter
            listUsers.forEach { it ->
                val user = it
                var selected = false
                selectedUsers.forEach {
                    if(it.uid == user.uid){
                        selected = true
                    }
                }
                membersList.forEach {
                    if(it == user.uid) selected = true
                }
                if(!selected){
                    adapter.add(
                        UserItemGroup(
                            (user),
                            recyclerview,
                            false
                        )
                    )
                }
            }
        }

    }
    private var group: Group? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_group_users)
        supportActionBar?.title = "Seleziona Partecipanti"
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mContext = this
        group = intent.getParcelableExtra("GROUP_KEY")
        horizontal_recyclerview.adapter = adapter_add
        vertical_recyclerview.adapter = adapter
        fetchMembers()
        fetchUser()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.new_group_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId){
            android.R.id.home -> {
                adapter.clear()
                adapter_add.clear()
                listUsers.clear()
                membersList.clear()
                selectedUsers.clear()
                finish()
            }

            R.id.menu_save -> {
                if(selectedUsers.isNotEmpty()){
                    selectedUsers.forEach {
                        FirebaseDatabase.getInstance().getReference("groups-users")
                            .child(group?.id!!)
                            .child(it.uid)
                            .setValue(it.uid)

                        val chatMessage = ChatMessage(
                            "default",
                            "Benvenuto in questo nuovo gruppo!",
                            "",
                            group?.id!!,
                            System.currentTimeMillis() / 1000,
                            ""
                        )

                        FirebaseDatabase.getInstance().getReference("group-latest-messages/${it.uid}")
                            .child(group?.id!!)
                            .setValue(chatMessage)
                    }

                    if(selectedUsers.count() > 1){
                        Toast.makeText(this, "Utenti aggiunti", Toast.LENGTH_SHORT)
                            .show()
                    } else{
                        Toast.makeText(this, "Utente aggiunto", Toast.LENGTH_SHORT)
                            .show()
                    }

                    adapter.clear()
                    adapter_add.clear()
                    listUsers.clear()
                    membersList.clear()
                    selectedUsers.clear()
                    GroupSettings.fetchUser(true)
                    finish()
                } else{
                    Toast.makeText(this, "Nessun utente selezionato", Toast.LENGTH_SHORT)
                        .show()
                }
            }

            R.id.menu_search -> {
                val searchView = item.actionView as androidx.appcompat.widget.SearchView
                searchView.queryHint = "Cerca utente..."
                searchView.setOnQueryTextListener(object : androidx.appcompat.widget.SearchView.OnQueryTextListener {
                    override fun onQueryTextChange(newText: String): Boolean {
                        if(newText != ""){
                            filterUsers(newText)
                        }else{
                            fetchUser()
                        }
                        return false
                    }

                    override fun onQueryTextSubmit(query: String): Boolean {
                        return false
                    }
                })
            }
        }
        return false
    }

    private fun fetchMembers(){
        val ref = FirebaseDatabase.getInstance().getReference("/groups-users/${group?.id}")
        ref.addValueEventListener(object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val data: ArrayList<String> = ArrayList()
                for (postSnapshot in dataSnapshot.children) {
                    val user: String? = postSnapshot.getValue(String::class.java)
                    if(user != null){
                        data.add(user)
                    }
                }
                membersList.clear()
                membersList.addAll(data)
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("GroupFragment", "loadPost:onCancelled", databaseError.toException())
            }
        })
    }

    private fun fetchUser() {
        val ref = FirebaseDatabase.getInstance().getReference("/users")
        ref.addValueEventListener(object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val data: ArrayList<User> = ArrayList()
                for (postSnapshot in dataSnapshot.children) {
                    val user: User? = postSnapshot.getValue(User::class.java)
                    if(user != null){
                        if(FirebaseAuth.getInstance().uid != user.uid){
                            data.add(user)
                        }
                    }
                }
                listUsers.clear()
                listUsers.addAll(data)
                refreshUsersList(horizontal_recyclerview)
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("GroupFragment", "loadPost:onCancelled", databaseError.toException())
            }
        })
    }

    //metodo che filtra gli utenti in base al campo di ricerca
    private fun filterUsers(filter:String) {
        //ottengo il riferimento a project in Firebase
        val ref = FirebaseDatabase.getInstance().getReference("/users")
        //aggiungo un Listener
        ref.addValueEventListener(object : ValueEventListener {

            @SuppressLint("DefaultLocale")
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val data: ArrayList<User> = ArrayList()
                for (postSnapshot in dataSnapshot.children) {
                    val item: User? = postSnapshot.getValue(User::class.java)
                    if (item != null && FirebaseAuth.getInstance().uid != item.uid) {
                        //se il campo di ricerca è contenuto nei campi dell'oggetto
                        if (item.username.toUpperCase().contains(filter.toUpperCase()) || item.email.toUpperCase().contains(filter.toUpperCase()))
                        //aggiungo l'oggetto alla lista
                            data.add(item)
                    }
                    listUsers.clear()
                    listUsers.addAll(data)
                    //aggiorno la schermata della ricerca
                    refreshUsersList(horizontal_recyclerview)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("GroupFragment", "loadPost:onCancelled", databaseError.toException())
            }
        })
    }
}

