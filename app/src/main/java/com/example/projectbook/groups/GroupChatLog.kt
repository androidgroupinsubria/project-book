package com.example.projectbook.groups

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.projectbook.ImageEditing
import com.example.projectbook.models.ChatMessage
import com.example.projectbook.models.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.activity_chat_log.*
import java.text.SimpleDateFormat
import java.util.*
import com.example.projectbook.R
import com.example.projectbook.main.MainActivity
import com.example.projectbook.models.Group
import com.example.projectbook.registerlogin.MY_PERMISSION_READ_EXTERNAL_STORAGE
import com.example.projectbook.views.*
import com.google.firebase.storage.FirebaseStorage
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.net.URL
import kotlin.collections.ArrayList

class GroupChatLog : AppCompatActivity() {
    //variabile che contiene i dati dell'utente
    companion object UserData {
        private var mContext: Context? = null
        fun getContext(): Context? {
            return mContext
        }
    }
    val adapter = GroupAdapter<GroupieViewHolder>()
    var group: Group? = null
    var userIdList : ArrayList<String> = ArrayList()
    var uri:String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mContext = this
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        setContentView(R.layout.activity_chat_log)
        recyclerview_chat_log.adapter = adapter
        group = intent.getParcelableExtra("GROUP_KEY")
        fetchUserIdList()
        supportActionBar!!.title = ""
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        //metodo che resta in ascolto dei messaggi
        listenForMessages()
        //listener del bottone di invio
        send_button_chat_log.setOnClickListener {
            performSendMessage("text")
            edittext_chat_log.setText("")
        }

        upload_image_chat.setOnClickListener {
            selectImage()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if(item!!.itemId==android.R.id.home) {
            finish()
        }

        if(item.itemId==R.id.profile_photo_chat_id || item.itemId.equals(R.id.profile_name_chat_id)) {
            val intent = Intent(this, GroupSettings::class.java)
            intent.putExtra("GROUP_KEY",group)
            startActivity(intent)
            return super.onOptionsItemSelected(item)
        }
        return false
    }

    private fun fetchUserIdList(){
        val ref = FirebaseDatabase.getInstance().getReference("/groups-users/${group?.id}")
        ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val data: ArrayList<String> = ArrayList()
                for (postSnapshot in dataSnapshot.children) {
                    val user = postSnapshot.getValue(String::class.java)
                    if(user != null){
                        data.add(user)
                    }
                }
                userIdList.clear()
                userIdList.addAll(data)
            }

            override fun onCancelled(p0: DatabaseError) {

            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.profile_photo_chat, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        var myImage: Bitmap?=null
        var myDrawable : Drawable?=null
        //recupero l'immagine dell'utente
        this.runOnUiThread {
            run {
                try {
                    val url = URL(group?.groupImage)
                    myImage = BitmapFactory.decodeStream(url.openConnection().getInputStream())
                } catch (e: IOException) {
                    println(e)
                }
                myDrawable = BitmapDrawable(this.resources, myImage)
            }}
        invalidateOptionsMenu()
        menu.findItem(R.id.profile_photo_chat_id).icon = myDrawable
        menu.findItem(R.id.profile_name_chat_id).title = group?.title
        return super.onPrepareOptionsMenu(menu)
    }

    private fun listenForMessages() {
        //riferimento dei messaggi inviati
        val ref = FirebaseDatabase.getInstance().getReference("/groups-messages/${FirebaseAuth.getInstance().uid}/${group?.id}")
        //listener per i figli
        ref.addChildEventListener(object: ChildEventListener {
            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                //oggetto di tipo ChatMessage
                val chatMessage = p0.getValue(ChatMessage::class.java)
                if(chatMessage != null && chatMessage.id != "default") {
                    if(chatMessage.fromId == FirebaseAuth.getInstance().uid){
                        if(chatMessage.type.equals("image")){
                            adapter.add(ChatFromItemImage(chatMessage.text, chatMessage.fromId, getDate(chatMessage.timeStamp)!!,getContext()!!))
                        } else if(chatMessage.type == "text"){
                            adapter.add(ChatFromItem(chatMessage.text, chatMessage.fromId, getDate(chatMessage.timeStamp)!!,getContext()!!))
                        }
                    } else{
                        if(chatMessage.type.equals("image")){
                            adapter.add(ChatToItemImage(chatMessage.text, chatMessage.fromId,getDate(chatMessage.timeStamp)!!,getContext()!!))
                        }else if(chatMessage.type.equals("text")){
                            adapter.add(ChatToItem(chatMessage.text, chatMessage.fromId,getDate(chatMessage.timeStamp)!!,getContext()!!))
                        }
                    }
                }
                recyclerview_chat_log.scrollToPosition(adapter.itemCount - 1)
            }

            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {

            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {

            }

            override fun onChildRemoved(p0: DataSnapshot) {

            }
        })
    }

    private fun performSendMessage(type:String) {

        //se il messaggio è un'immagine
        if(type == "image"){
            //se l'uri non è vuoto
            if(uri.isNotEmpty()){
                val text = uri
                val fromId = FirebaseAuth.getInstance().uid
                val key = FirebaseDatabase.getInstance().getReference("/groups-messages/$fromId/${group?.id}").push().key

                val chatMessage = ChatMessage(
                    key!!,
                    text,
                    fromId!!,
                    group!!.id,
                    System.currentTimeMillis() / 1000,
                    type
                )

                userIdList.forEach {
                    val reference =
                        FirebaseDatabase.getInstance().getReference("/groups-messages/${it}/${group?.id}/$key")

                    reference.setValue(chatMessage)

                    val latestMessageToRef =
                        FirebaseDatabase.getInstance().getReference("/group-latest-messages/${it}/${group?.id}")
                    latestMessageToRef.setValue(chatMessage)
                }
            }
        }else {

            //se il messaggio non è vuoto
            if (edittext_chat_log.text.toString().isNotEmpty()) {
                val text = edittext_chat_log.text.toString()
                val fromId = FirebaseAuth.getInstance().uid
                val key = FirebaseDatabase.getInstance().getReference("/groups-messages/$fromId/${group?.id}").push().key

                val chatMessage = ChatMessage(
                    key!!,
                    text,
                    fromId!!,
                    group!!.id,
                    System.currentTimeMillis() / 1000,
                    type
                )

                userIdList.forEach {
                    val reference =
                        FirebaseDatabase.getInstance().getReference("/groups-messages/${it}/${group?.id}/$key")

                    reference.setValue(chatMessage)

                    val latestMessageToRef =
                        FirebaseDatabase.getInstance().getReference("/group-latest-messages/${it}/${group?.id}")
                    latestMessageToRef.setValue(chatMessage)
                }

            }
        }
    }


    //metodo che recupera l'ora dell'inivio del messaggio
    @SuppressLint("SimpleDateFormat")
    fun getDate(timestamp: Long): String? {
        try {
            val calendar: Calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"), Locale.getDefault())
            val tz: TimeZone = TimeZone.getDefault()
            calendar.timeInMillis = (timestamp-7200) * 1000
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.timeInMillis))
            val sdf = SimpleDateFormat("h:mm a")
            val timeZone: Date = calendar.time as Date
            return sdf.format(timeZone)
        } catch (e: Exception) {
        }
        return ""
    }

    fun selectImage(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            when {
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE) -> {
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                }
                else -> {
                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(this,
                        arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                        MY_PERMISSION_READ_EXTERNAL_STORAGE
                    )

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            }
        } else {
            // Permission has already been granted
            val intent= Intent(Intent.ACTION_PICK)
            intent.type= "image/*"
            //apro selezionatore di immagini
            startActivityForResult(intent, 0)
        }
    }

    private var selectedPhotoUri: Uri? =null
    var bitmap : Bitmap? =null
    //quando ho selezionato l'immagine
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        if(requestCode==0 && resultCode== Activity.RESULT_OK && data !=null){
            // controllo quale immagine è stata selezionata

            selectedPhotoUri=data.data


            //la salvo in una variabile
            bitmap= MediaStore.Images.Media.getBitmap(contentResolver,selectedPhotoUri)

            //ottengo l'orientamento originale dell'immagine

            val orientation = ExifInterface(getPath(selectedPhotoUri)!!).getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED
            )

            //ruoto l'immagine nella posizione corretta
            bitmap = ImageEditing.rotateBitmap(bitmap!!, orientation)

            //carico l'immagine su firebase
            uploadImageToFirebaseStorage()

        }
    }

    private fun uploadImageToFirebaseStorage(){
        var data: ByteArray?=null
        //carico immagine su firebase storage
        val filename= UUID.randomUUID().toString()
        val ref= FirebaseStorage.getInstance().getReference("/images/$filename")
        if(bitmap ==null){
            this.runOnUiThread {
                run {
                    val stream = ByteArrayOutputStream()
                    var bitmap: Bitmap =
                        BitmapFactory.decodeResource(resources, R.drawable.project)
                    bitmap = Bitmap.createScaledBitmap(bitmap, 200, 200, true)
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
                    data = stream.toByteArray()
                }}
        }else{
            this.runOnUiThread {
                run {
                    bitmap = Bitmap.createScaledBitmap(bitmap!!, 200, 200, true)
                    val baos = ByteArrayOutputStream()
                    bitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                    data = baos.toByteArray()
                }}
        }


        ref.putBytes(data!!)
            .addOnSuccessListener {
                ref.downloadUrl.addOnSuccessListener {
                    uri=it.toString()
                    performSendMessage("image")
                }
            }
            .addOnFailureListener {

            }
    }

    //ottengo la directory dell'immagine
    fun getPath(uri: Uri?): String? {
        val projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = uri?.let { contentResolver.query(it, projection, null, null, null) } ?: return null
        val columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        val s = cursor.getString(columnIndex)
        cursor.close()
        return s
    }



    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSION_READ_EXTERNAL_STORAGE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //permessi ottenuti
                    val intent= Intent(Intent.ACTION_PICK)
                    intent.type= "image/*"
                    //apro selezionatore di immagini
                    startActivityForResult(intent,0)
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }


}





