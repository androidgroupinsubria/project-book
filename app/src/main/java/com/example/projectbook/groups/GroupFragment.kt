package com.example.projectbook.groups

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.GroupAdapter
import com.example.projectbook.R
import com.example.projectbook.models.ChatMessage
import com.example.projectbook.views.GroupLatestMessageRow
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

class GroupFragment : Fragment() {

    companion object {
        private var mContext: Context? = null
        fun getContext(): Context? {
            return mContext
        }
    }

    //inizializzo l'adapter
    private val adapter = GroupAdapter<GroupieViewHolder>()
    val groupsMapNotSorted = LinkedHashMap<String, ChatMessage>()
    var groupsMessagesMap: Map<String, ChatMessage>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
        showAllGroups()
        mContext = context
    }

    override fun onResume() {
        super.onResume()
        showAllGroups()
        mContext = context
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.groups_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        //se l'utente pereme sul tasto di logout
        when (item.itemId) {
            R.id.menu_new_group -> {
                val intent = Intent(activity, NewGroup::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateView(

        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //ottengo la rootView
        val rootView = inflater.inflate(R.layout.fragment_group, container, false)
        val recyclerView = rootView.findViewById(R.id.recyclerview_group) as RecyclerView
        recyclerView.adapter = adapter
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                activity,
                DividerItemDecoration.VERTICAL
            )
        )
        adapter.setOnItemClickListener { item, _ ->
            val intent = Intent(activity, GroupChatLog::class.java)
            val row = item as GroupLatestMessageRow
            if(row.group!=null) {
                intent.putExtra("GROUP_KEY", row.group)
                startActivity(intent)
            }
        }
        return rootView
    }

    private fun refreshRecyclerViewMessages() {
        adapter.clear()
        groupsMessagesMap!!.values.forEach {
            adapter.add(GroupLatestMessageRow(it))
        }
    }

    private fun showAllGroups() {
        val myId = FirebaseAuth.getInstance().uid
        val ref = FirebaseDatabase.getInstance().getReference("/group-latest-messages/$myId")
        ref.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                val groupMessage: ChatMessage = p0.getValue(ChatMessage::class.java) ?: return
                groupsMapNotSorted[p0.key!!] = groupMessage
                groupsMessagesMap = groupsMapNotSorted.toList()
                    .sortedByDescending { (_, value) -> value.timeStamp }
                    .toMap()

                refreshRecyclerViewMessages()
            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                val groupMessage: ChatMessage = p0.getValue(ChatMessage::class.java) ?: return
                groupsMapNotSorted[p0.key!!] = groupMessage
                groupsMessagesMap = groupsMapNotSorted.toList()
                    .sortedByDescending { (_, value) -> value.timeStamp }
                    .toMap()

                refreshRecyclerViewMessages()
            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
            }

            override fun onChildRemoved(p0: DataSnapshot) {
            }

            override fun onCancelled(p0: DatabaseError) {

            }
        })
    }
}

