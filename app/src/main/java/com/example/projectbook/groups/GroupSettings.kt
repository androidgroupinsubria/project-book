package com.example.projectbook.groups

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.projectbook.R
import com.example.projectbook.main.MainActivity
import com.example.projectbook.messages.ChatLog
import com.example.projectbook.models.Group
import com.example.projectbook.models.User
import com.example.projectbook.views.UserItem
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.group_settings.*
import java.lang.Exception

class GroupSettings : AppCompatActivity() {

    companion object UserData {
        private val adapter = GroupAdapter<GroupieViewHolder>()
        private var group: Group? = null
        var listIdUsers: ArrayList<String> = ArrayList()

        fun deleteItem(item: Item<GroupieViewHolder>, user: User){
            try{
                adapter.remove(item)
                FirebaseDatabase.getInstance().getReference("groups-users")
                    .child(group?.id!!)
                    .child(user.uid)
                    .removeValue()
                FirebaseDatabase.getInstance().getReference("group-latest-messages/${user.uid}")
                    .child(group?.id!!)
                    .removeValue()
                FirebaseDatabase.getInstance().getReference("groups-messages/${user.uid}")
                    .child(group?.id!!)
                    .removeValue()
            }catch (e: Exception){}
        }

        fun fetchUser(deletable: Boolean) {
            adapter.clear()
            val ref = FirebaseDatabase.getInstance().getReference("/groups-users/${group?.id}")
            ref.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(p0: DataSnapshot) {
                    p0.children.forEach {
                        val user = it.getValue(String::class.java)
                        if (user != null) {
                            listIdUsers.add(user)

                            if(user != FirebaseAuth.getInstance().uid){
                                adapter.add(UserItem(user, deletable))
                            } else{
                                adapter.add(UserItem(user, false))
                            }
                        }
                    }
                }

                override fun onCancelled(p0: DatabaseError) {

                }
            })
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.group_settings)
        recyclerview_groupsettings.adapter = adapter
        adapter.setOnItemClickListener { item, view ->
            val userItem = item as UserItem
            val intent = Intent(view.context, ChatLog::class.java)
            intent.putExtra("USER_KEY", userItem.user)
            startActivity(intent)
        }
        supportActionBar?.title = getString(R.string.user_group)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        group = intent.getParcelableExtra("GROUP_KEY")

        if (group?.authorId.equals(FirebaseAuth.getInstance().uid)) {
            fetchUser(true)
            delete_group.text = getString(R.string.elimina_gruppo)
            delete_group.setOnClickListener {
                FirebaseDatabase.getInstance().getReference("groups")
                    .child(group?.id!!)
                    .removeValue()
                FirebaseDatabase.getInstance().getReference("groups-users")
                    .child(group?.id!!)
                    .removeValue()
                for(user in listIdUsers){
                    FirebaseDatabase.getInstance().getReference("group-latest-messages/${user}")
                        .child(group?.id!!)
                        .removeValue()
                    FirebaseDatabase.getInstance().getReference("groups-messages/${user}")
                        .child(group?.id!!)
                        .removeValue()
                }
                finish()
                val intent = Intent(this, MainActivity::class.java)
                intent.putExtra("GROUP_KEY", false)
                startActivity(intent)
            }
            add_users.visibility = View.VISIBLE
            add_users.setOnClickListener{
                val intent = Intent(this, AddGroupUsers::class.java)
                intent.putExtra("GROUP_KEY", group)
                startActivity(intent)
            }
        } else{
            fetchUser(false)
            add_users.visibility = View.GONE
            delete_group.text = getString(R.string.abbandona_gruppo)
            delete_group.setOnClickListener {
                FirebaseDatabase.getInstance().getReference("groups-users")
                    .child(group?.id!!)
                    .child(FirebaseAuth.getInstance().uid!!)
                    .removeValue()
                FirebaseDatabase.getInstance().getReference("group-latest-messages/${FirebaseAuth.getInstance().uid}")
                    .child(group?.id!!)
                    .removeValue()
                FirebaseDatabase.getInstance().getReference("groups-messages/${FirebaseAuth.getInstance().uid}")
                    .child(group?.id!!)
                    .removeValue()
                finish()
                val intent = Intent(this, MainActivity::class.java)
                intent.putExtra("GROUP_KEY", false)
                startActivity(intent)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId){
            android.R.id.home -> {
                listIdUsers.clear()
                adapter.clear()
                finish()
            }
        }
        return false
    }

}