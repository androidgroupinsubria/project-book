package com.example.projectbook.groups

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ExifInterface
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.projectbook.ImageEditing
import com.example.projectbook.R
import com.example.projectbook.models.ChatMessage
import com.example.projectbook.models.Group
import com.example.projectbook.models.User
import com.example.projectbook.registerlogin.MY_PERMISSION_READ_EXTERNAL_STORAGE
import com.example.projectbook.views.SelectedUserItem
import com.example.projectbook.views.UserItemGroup
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.activity_new_group.*
import java.io.ByteArrayOutputStream
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class NewGroup : AppCompatActivity() {

    companion object UserData {
        var selectedUsers: ArrayList<User> = ArrayList()
        var listUsers: ArrayList<User> = ArrayList()

        private val adapter = GroupAdapter<GroupieViewHolder>()
        private val adapter_add = GroupAdapter<GroupieViewHolder>()

        private var mContext: Context? = null
        fun getContext(): Context? {
            return mContext
        }

        fun addItem(item: Item<GroupieViewHolder>, recyclerview: RecyclerView) {
            try{
                adapter.remove(item)
                val userItem = item as UserItemGroup
                adapter_add.add(0,SelectedUserItem(userItem.user, recyclerview, true))
            }catch (e:Exception){}

        }

        fun addUserToList(user:User){
            selectedUsers.add(user)
        }

        fun removeUserToList(user:User){
            selectedUsers.remove(user)
        }

        fun removeItem(item: Item<GroupieViewHolder>, recyclerview: RecyclerView) {
            try {
                adapter_add.remove(item)
                refreshUsersList(recyclerview)
            }catch (e:Exception){}

        }

        fun getAdapterCount(): Int {
            return adapter_add.itemCount
        }

        fun refreshUsersList(recyclerview: RecyclerView){
            //pulisci l'adapter
            adapter.clear()
            //aggiungo gli oggetti all'adapter
            listUsers.forEach { it ->
                val user = it
                var selected = false
                selectedUsers.forEach {
                    if(it.uid.equals(user.uid)){
                        selected = true
                    }
                }
                if(!selected){
                    adapter.add(
                        UserItemGroup(
                            (user),
                            recyclerview,
                            true
                        )
                    )
                }
            }
        }

    }

    private var currentUser : User? = null

    private var selectedPhotoUri: Uri? =null
    private var bitmap : Bitmap? =null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setContentView(R.layout.activity_new_group)
        mContext = this
        users_recyclerview.adapter = adapter
        users_added_recyclerview.adapter = adapter_add

        group_image.setOnClickListener {
            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

                // Permission is not granted
                // Should we show an explanation?
                when {
                    ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE) -> {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
                    }
                    else -> {
                        // No explanation needed, we can request the permission.

                        ActivityCompat.requestPermissions(this,
                            arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                            MY_PERMISSION_READ_EXTERNAL_STORAGE
                        )

                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                }
            } else {
                // Permission has already been granted
                val intent= Intent(Intent.ACTION_PICK)
                intent.type= "image/*"
                //apro selezionatore di immagini
                startActivityForResult(intent,0)
            }
        }

        fetchUser()
    }

    //quando ho selezionato l'immagine
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode==0 && resultCode== Activity.RESULT_OK && data !=null){
            // controllo quale immagine è stata selezionata

            selectedPhotoUri=data.data


            //la salvo in una variabile
            bitmap= MediaStore.Images.Media.getBitmap(contentResolver,selectedPhotoUri)

            //ottengo l'orientamento originale dell'immagine

            val orientation = ExifInterface(getPath(selectedPhotoUri)!!).getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED
            )

            //ruoto l'immagine nella posizione corretta
            bitmap = ImageEditing.rotateBitmap(bitmap!!, orientation)

            group_image.setImageBitmap(bitmap)
        }
    }

    //ottengo la directory dell'immagine
    fun getPath(uri: Uri?): String? {
        val projection =
            arrayOf(MediaStore.Images.Media.DATA)
        val cursor = uri?.let { contentResolver.query(it, projection, null, null, null) } ?: return null
        val columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        val s = cursor.getString(columnIndex)
        cursor.close()
        return s
    }

    private fun fetchUser() {
        val ref = FirebaseDatabase.getInstance().getReference("/users")
        ref.addValueEventListener(object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val data: ArrayList<User> = ArrayList()
                for (postSnapshot in dataSnapshot.children) {
                    val user: User? = postSnapshot.getValue(User::class.java)
                    if(user != null){
                        if(FirebaseAuth.getInstance().uid != user.uid){
                            data.add(user)
                        } else{
                            currentUser = user
                        }
                    }
                }
                listUsers.clear()
                listUsers.addAll(data)
                refreshUsersList(users_added_recyclerview)
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("SearchFragment", "loadPost:onCancelled", databaseError.toException())
            }
        })
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSION_READ_EXTERNAL_STORAGE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //permessi ottenuti
                    val intent= Intent(Intent.ACTION_PICK)
                    intent.type= "image/*"
                    //apro selezionatore di immagini
                    startActivityForResult(intent,0)
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }

    private fun uploadImageToFirebaseStorage(id: String, title: String){
        var data: ByteArray?=null
        //carico immagine su firebase storage
        val filename= UUID.randomUUID().toString()
        val ref= FirebaseStorage.getInstance().getReference("/images/$filename")
        if(bitmap ==null){
            this.runOnUiThread {
                run {
                    val stream = ByteArrayOutputStream()
                    var bitmap: Bitmap =
                        BitmapFactory.decodeResource(resources, R.drawable.project)
                    bitmap = Bitmap.createScaledBitmap(bitmap, 200, 200, true)
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
                    data = stream.toByteArray()
                }}
        }else{
            this.runOnUiThread {
                run {
                    bitmap = Bitmap.createScaledBitmap(bitmap!!, 200, 200, true)
                    val baos = ByteArrayOutputStream()
                    bitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                    data = baos.toByteArray()
                }}
        }


        ref.putBytes(data!!)
            .addOnSuccessListener {
                ref.downloadUrl.addOnSuccessListener {
                    val reference : DatabaseReference = FirebaseDatabase.getInstance().getReference("/groups/$id")
                    val group = Group(id, title, currentUser!!.uid, currentUser!!.username, it.toString())
                    reference.setValue(group)
                    finish()
                    adapter.clear()
                    adapter_add.clear()
                    val intent = Intent(this, GroupChatLog::class.java)
                    intent.putExtra("GROUP_KEY", group)
                    startActivity(intent)
                }
            }
            .addOnFailureListener {

            }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.new_group_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        //se l'utente pereme sul tasto di logout
        when (item?.itemId){
            android.R.id.home -> {
                adapter.clear()
                adapter_add.clear()
                selectedUsers.clear()
                finish()
            }

            R.id.menu_save -> {
                val txt = group_name_edit_text.text.toString()
                if(selectedUsers.isEmpty()) Toast.makeText(this,getString(R.string.seleziona_utenti),Toast.LENGTH_SHORT).show()
                if(txt.isNotEmpty() && selectedUsers.isNotEmpty()){
                    group_name_edit_text.setText("")
                    val ref : DatabaseReference = FirebaseDatabase.getInstance().getReference("/groups-users").push()
                    val id = ref.key
                    ref.child("/${currentUser?.uid}").setValue(currentUser?.uid)
                    selectedUsers.forEach {
                        val ref : DatabaseReference = FirebaseDatabase.getInstance().getReference("/groups-users/$id/${it.uid}")
                        ref.setValue(it.uid)
                    }
                    uploadImageToFirebaseStorage(id!!, txt)
                    performFirstMessage(id)
                    selectedUsers.clear()
                    adapter.clear()
                    adapter_add.clear()
                }
            }

            R.id.menu_search -> {
                val searchView = item.actionView as androidx.appcompat.widget.SearchView
                searchView.queryHint = getString(R.string.cerca_utente)
                searchView.setOnQueryTextListener(object : androidx.appcompat.widget.SearchView.OnQueryTextListener {
                    override fun onQueryTextChange(newText: String): Boolean {
                        if(!newText.equals("")){
                            filterUsers(newText)
                        }else{
                            fetchUser()
                        }
                        return false
                    }

                    override fun onQueryTextSubmit(query: String): Boolean {
                        return false
                    }
                })
            }
        }
        return super.onOptionsItemSelected(item)
    }

    //metodo che invia un messaggio di default nella chat di gruppo appena creata
    private fun performFirstMessage(id: String){
        val ref = FirebaseDatabase.getInstance().getReference("/groups-messages/${currentUser?.uid}/$id").push()
        val latestMessageToRef =
            FirebaseDatabase.getInstance().getReference("/group-latest-messages/${currentUser?.uid}/$id")

        val chatMessage = ChatMessage(
            "default",
            "Benvenuto in questo nuovo gruppo!",
            "",
            id,
            System.currentTimeMillis() / 1000,
            ""
        )

        ref.setValue(chatMessage)
        latestMessageToRef.setValue(chatMessage)

        selectedUsers.forEach {
            val ref = FirebaseDatabase.getInstance().getReference("/groups-messages/${it.uid}/$id").push()

            ref.setValue(chatMessage)

            val latestMessageToRef =
                FirebaseDatabase.getInstance().getReference("/group-latest-messages/${it.uid}/$id")
            latestMessageToRef.setValue(chatMessage)
        }
    }

    //metodo che filtra gli utenti in base al campo di ricerca
    private fun filterUsers(filter:String) {
        //ottengo il riferimento a project in Firebase
        val ref = FirebaseDatabase.getInstance().getReference("/users")
        //aggiungo un Listener
        ref.addValueEventListener(object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val data: ArrayList<User> = ArrayList()
                for (postSnapshot in dataSnapshot.children) {
                    val item: User? = postSnapshot.getValue(User::class.java)
                    if (item != null && currentUser?.uid != item.uid) {
                        //se il campo di ricerca è contenuto nei campi dell'oggetto
                        if (item.username.toUpperCase().contains(filter.toUpperCase()) || item.email.toUpperCase().contains(filter.toUpperCase()))
                        //aggiungo l'oggetto alla lista
                            data.add(item)
                    }
                    listUsers.clear()
                    listUsers.addAll(data)
                    //aggiorno la schermata della ricerca
                    refreshUsersList(users_added_recyclerview)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("GroupFragment", "loadPost:onCancelled", databaseError.toException())
            }
        })
    }


}
