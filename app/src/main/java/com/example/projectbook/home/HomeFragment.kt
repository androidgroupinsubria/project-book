package com.example.projectbook.home

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.projectbook.models.Project
import com.example.projectbook.project.ShowProject
import com.example.projectbook.views.ProjectRow
import com.google.firebase.database.*
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlin.collections.LinkedHashMap
import com.example.projectbook.R

class HomeFragment : Fragment() {
    private val adapter = GroupAdapter<GroupieViewHolder>()
    val projectHashMap = LinkedHashMap<String, Project>()
    var projectMap: Map<String, Project>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
        showAllProjects()
    }

    override fun onResume() {
        super.onResume()
        showAllProjects()
    }

    private fun showAllProjects() {
        projectHashMap.clear()
        val ref = FirebaseDatabase.getInstance().getReference("/projects")
        ref.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                val newProject = p0.getValue(Project::class.java) ?: return
                projectHashMap[p0.key!!] = newProject
                projectMap = projectHashMap.toList()
                    .sortedByDescending { (_, value) -> value.timeStamp }
                    .toMap()
                refreshRecyclerViewProjects()
            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                val newProject = p0.getValue(Project::class.java) ?: return
                projectHashMap[p0.key!!] = newProject
                projectMap = projectHashMap.toList()
                    .sortedByDescending { (_, value) -> value.timeStamp }
                    .toMap()
                refreshRecyclerViewProjects()
            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
            }

            override fun onChildRemoved(p0: DataSnapshot) {
                val newProject = p0.getValue(Project::class.java) ?: return
                projectHashMap[p0.key!!] = newProject
                projectMap = projectHashMap.toList()
                    .sortedByDescending { (_, value) -> value.timeStamp }
                    .toMap()
                refreshRecyclerViewProjects()
            }

            override fun onCancelled(p0: DatabaseError) {
            }
        })
    }

    private fun refreshRecyclerViewProjects() {
        if (projectMap != null) {
            adapter.clear()
            for ((key, value) in projectMap!!) {
                adapter.add(ProjectRow(value, key))
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.home_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_search -> {
                val searchView = item.actionView as androidx.appcompat.widget.SearchView
                searchView.queryHint = getString(R.string.cerca_progetto)

                searchView.setOnQueryTextListener(object :
                    androidx.appcompat.widget.SearchView.OnQueryTextListener {
                    override fun onQueryTextChange(newText: String): Boolean {
                        filterProjects(newText)
                        if (newText == "") {
                            showAllProjects()
                        }
                        return false
                    }

                    override fun onQueryTextSubmit(query: String): Boolean {
                        return false
                    }
                })
            }


        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_home, container, false)
        val recyclerView = rootView.findViewById(R.id.recyclerview_home) as RecyclerView
        recyclerView.adapter = adapter
        adapter.setOnItemClickListener { item, _ ->
            val intent = Intent(activity, ShowProject::class.java)
            val row = item as ProjectRow
            val bundle = Bundle()
                bundle.putParcelable("PROJECT", row.project)
                bundle.putString("KEY", row.key)
                intent.putExtras(bundle)
                startActivity(intent)
        }
        return rootView
    }

    //metodo che filtra i progetti in base ad un filtro(progetto,utente) e al campo di ricerca
    private fun filterProjects(filter: String) {
        //pulisco l'hashmap
        projectHashMap.clear()
        //ottengo il riferimento a project in Firebase
        val ref = FirebaseDatabase.getInstance().getReference("/projects")
        //aggiungo un Listener
        ref.addValueEventListener(object : ValueEventListener {

            @SuppressLint("DefaultLocale")
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (postSnapshot in dataSnapshot.children) {
                    val item: Project? = postSnapshot.getValue(Project::class.java)
                    if (item != null) {
                        //se il campo di ricerca è contenuto nei campi dell'oggetto
                        if (item.title.toUpperCase().contains(filter.toUpperCase()) || item.desc.toUpperCase().contains(
                                filter.toUpperCase()
                            )
                        )
                        //aggiungo l'oggetto alla lista
                        projectHashMap[postSnapshot.key!!] = item
                    }
                }
                projectMap = projectHashMap.toList()
                    .sortedByDescending { (_, value) -> value.timeStamp }
                    .toMap()
                refreshRecyclerViewProjects()
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("SearchFragment", "loadPost:onCancelled", databaseError.toException())
            }
        })
    }
}



