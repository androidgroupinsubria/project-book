package com.example.projectbook.main


import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentTransaction
import com.example.projectbook.database.DatabaseHelper
import com.example.projectbook.home.HomeFragment
import com.example.projectbook.messages.MessagesFragment
import com.example.projectbook.models.User
import com.example.projectbook.profile.Profile
import com.example.projectbook.project.Addproject
import com.example.projectbook.project.Myproject
import com.example.projectbook.groups.GroupFragment
import com.example.projectbook.service.MyIntentService
import com.example.projectbook.welcome.WelcomeActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import com.example.projectbook.R

class MainActivity : AppCompatActivity() {

    //inizializzo variabili per menù a comparsa laterale
    private var mDrawerlayout: DrawerLayout? = null
    private var mToogle: ActionBarDrawerToggle? = null

    //inizializzo i Fragments
    private lateinit var homeFragment: HomeFragment
    private lateinit var messagesFragment: MessagesFragment
    private lateinit var groupFragment: GroupFragment

    //inizializzo variabile che conterrà i dati dell'user di tipo User
    var user: User? = null
    var isDefault: Boolean? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //imposto il layout dell'activity
        setContentView(R.layout.activity_main)
        isDefault = intent.getBooleanExtra("GROUP_KEY", true)
        //faccio partire il service che resterà in ascolto di nuovi messaggi
        val intent = Intent(this, MyIntentService::class.java)
        startService(intent)
        //aggiungo menù a comparsa laterale
        mDrawerlayout = findViewById(R.id.drawer)
        mToogle = ActionBarDrawerToggle(this, mDrawerlayout, R.string.open, R.string.close)
        mDrawerlayout!!.addDrawerListener(mToogle!!)
        mToogle!!.syncState()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        //verifico se l'utente è loggato
        verifyUserIsLoggedIn()

        //imposto il menu di navigazione in basso
        val bottomNavigation: BottomNavigationView = findViewById(R.id.bottom_nav)
        //imposto il fragment di default
        if (isDefault as Boolean) {
            setDefaultFragment()
        } else {
            setGroupFragment()
            bottomNavigation.selectedItemId = R.id.nav_group
        }

        //imposto il listener per il menù in basso
        setListener(bottomNavigation)
        //imposto il menù in alto
        val upMenu: NavigationView = findViewById(R.id.nav_view)
        setListener2(upMenu)
    }


    override fun onResume() {
        super.onResume()
        //verifico se l'utente è loggato
        verifyUserIsLoggedIn()

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        //apre e chiude il menù a comparsa laterale
        if (mToogle!!.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item!!)
    }

    //imposto il Fragment di dafault a HomeFragment
    private fun setDefaultFragment() {
        homeFragment = HomeFragment()
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frame_layout, homeFragment)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()
    }

    private fun setGroupFragment() {
        groupFragment = GroupFragment()
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frame_layout, groupFragment)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()
    }

    //aggiungo listener sui bottoni del menù in basso
    private fun setListener(btmNav: BottomNavigationView) {
        btmNav.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                //se clicco su home imposto il fragment home
                R.id.nav_home -> {
                    //controllo se il fragment non sia già selezionato
                    if (!supportFragmentManager.findFragmentById(R.id.frame_layout)?.tag.equals("home")) {
                        homeFragment = HomeFragment()
                        supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.frame_layout, homeFragment, "home")
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .commit()
                    }

                }

                //se clicco su messaggi imposto il fragment messaggi
                R.id.nav_messages -> {
                    //controllo se il fragment non sia già selezionato
                    if (!supportFragmentManager.findFragmentById(R.id.frame_layout)?.tag.equals("messages")) {
                        messagesFragment = MessagesFragment()
                        supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.frame_layout, messagesFragment, "messages")
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .commit()
                    }
                }

                //se clicco su group imposto il fragment group
                R.id.nav_group -> {
                    //controllo se il fragment non sia già selezionato
                    if (!supportFragmentManager.findFragmentById(R.id.frame_layout)?.tag.equals("group")) {
                        groupFragment = GroupFragment()
                        supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.frame_layout, groupFragment, "group")
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .commit()
                    }
                }
            }
            true
        }
    }

    //imposto il listener per il menù a comparsa laterale
    private fun setListener2(btmNav: NavigationView) {
        btmNav.setNavigationItemSelectedListener { item ->
            //se clicco sul bottone profilo
            when (item.itemId) {
                R.id.profile -> {
                    //se l'user non è null
                    if (user != null) {
                        //chiudo il menu
                        mDrawerlayout!!.closeDrawer(GravityCompat.START)
                        //apro la schermata del profilo passando i dati dell'user
                        val intent = Intent(this, Profile::class.java)
                        intent.putExtra("USER_KEY", user)
                        startActivity(intent)
                    }
                }

                R.id.myproject -> {
                    //chiudo il menu
                    mDrawerlayout!!.closeDrawer(GravityCompat.START)
                    //apro la schermata dei progetti
                    val intent = Intent(this, Myproject::class.java)
                    startActivity(intent)
                }

                R.id.newproject -> {
                    //chiudo il menu
                    mDrawerlayout!!.closeDrawer(GravityCompat.START)
                    //apro la schermata della definizione del progetto
                    val intent = Intent(this, Addproject::class.java)
                    intent.putExtra("USER_KEY", user)
                    startActivity(intent)
                }
                R.id.logout -> {
                    //chiudo il menu
                    mDrawerlayout!!.closeDrawer(GravityCompat.START)
                    FirebaseAuth.getInstance().signOut()
                    val intent = Intent(this, WelcomeActivity::class.java)
                    startActivity(intent)
                }


            }
            true
        }
    }

    //verifico se l'utente è loggato
    private fun verifyUserIsLoggedIn() {
        //recupero l'id dell'utente in Firebase
        val uid = FirebaseAuth.getInstance().uid
        //se è null allora rimando l'utente alla schermata di benvenuto
        if (uid == null) {
            val intent = Intent(this, WelcomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        } else {
            //immagine e nome dell'user nel menù laterale
            val navigationView = findViewById<NavigationView>(R.id.nav_view)
            val headerView: View = navigationView.getHeaderView(0)
            val navUsername = headerView.findViewById(R.id.profilename) as TextView
            val userphoto = headerView.findViewById(R.id.profileimage) as ImageView
            //altrimenti recupero i dati dell'utente nel DB locale se esistono o in Firebase altrimenti
            val helper = DatabaseHelper(this)
            //se l'utente esiste nel DB locale
            val map: HashMap<String, String>? = helper.getUserSettings(uid)
            var username = ""
            var photo = ""
            for ((key, value) in map!!) {
                username = key
                photo = value
            }
            //se i dati sono presenti nel DB locale
            if (username != "" && photo != "") {
                //recupero comunque il riferimento dell'utente di firebase
                val ref = FirebaseDatabase.getInstance().getReference("users")
                    .child(FirebaseAuth.getInstance().uid!!)
                val menuListener = object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        user = dataSnapshot.getValue(User::class.java)
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        // handle error
                    }
                }
                ref.addListenerForSingleValueEvent(menuListener)
                //imposto l'username e la foto con i dati presenti nel db locale
                navUsername.text = username
                Picasso.get().load(photo).placeholder(R.drawable.progress_animation).into(userphoto)
            }
            //altrimenti
            else {
                //recupero il riferimento dell'utente di firebase
                val ref = FirebaseDatabase.getInstance().getReference("users")
                    .child(FirebaseAuth.getInstance().uid!!)
                val menuListener = object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        user = dataSnapshot.getValue(User::class.java)
                        //imposto i dati dell'utente nel menu a comparsa
                        //imposto username
                        navUsername.text = user!!.username
                        //imposto immagine
                        Picasso.get().load(user!!.profileImageUrl)
                            .placeholder(R.drawable.progress_animation).into(userphoto)
                        //inserisco i dati nel DB locale
                        helper.insertUserSettings(
                            uid,
                            user!!.email,
                            user!!.username,
                            user!!.profileImageUrl
                        )
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        // handle error
                    }
                }
                ref.addListenerForSingleValueEvent(menuListener)

            }
        }
    }

}

