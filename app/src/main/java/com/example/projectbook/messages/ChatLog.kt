package com.example.projectbook.messages

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.provider.MediaStore.Images.Media.DATA
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.projectbook.ImageEditing
import com.example.projectbook.models.ChatMessage
import com.example.projectbook.models.User
import com.example.projectbook.profile.Profile
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.activity_chat_log.*
import java.io.IOException
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import com.example.projectbook.R
import com.example.projectbook.registerlogin.MY_PERMISSION_READ_EXTERNAL_STORAGE
import com.example.projectbook.views.ChatFromItem
import com.example.projectbook.views.ChatFromItemImage
import com.example.projectbook.views.ChatToItem
import com.example.projectbook.views.ChatToItemImage
import com.google.firebase.storage.FirebaseStorage
import java.io.ByteArrayOutputStream
import kotlin.String as String1


class ChatLog : AppCompatActivity() {
    //variabile che contiene i dati dell'utente
    companion object UserData {
        private var mContext: Context? = null
        var user2: User? =null
        fun getContext(): Context? {
            return mContext
        }
    }

    val adapter = GroupAdapter<GroupieViewHolder>()
    var toUser: User? = null
    var uri: kotlin.String =""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mContext = this
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        setContentView(R.layout.activity_chat_log)
        recyclerview_chat_log.adapter = adapter
        toUser = intent.getParcelableExtra("USER_KEY")
        supportActionBar!!.title = ""
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        //metodo che resta in ascolto dei messaggi
        listenForMessages()
        //listener del bottone di invio
        send_button_chat_log.setOnClickListener {
            performSendMessage("text")
            edittext_chat_log.setText("")
        }

        upload_image_chat.setOnClickListener {
            selectImage()
        }

    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.profile_photo_chat, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        var myImage: Bitmap?=null
        var myDrawable : Drawable?=null
        val url = URL(toUser!!.profileImageUrl)
        //recupero l'immagine dell'utente
        this.runOnUiThread {
            run {
                try {
                    myImage = BitmapFactory.decodeStream(url.openConnection().getInputStream())
                    myDrawable = BitmapDrawable(this.resources, myImage)
                } catch (e: IOException) {
                    println(e)
                }
            }
        }

        invalidateOptionsMenu()
        menu.findItem(R.id.profile_photo_chat_id).icon = myDrawable
        menu.findItem(R.id.profile_name_chat_id).title = toUser!!.username
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item!!.itemId==android.R.id.home) {
            finish()
        }

        if(item.itemId==R.id.profile_photo_chat_id || item.itemId.equals(R.id.profile_name_chat_id)) {
            val intent = Intent(this, Profile::class.java)
            intent.putExtra("USER_KEY", toUser)
            startActivity(intent)
            return super.onOptionsItemSelected(item)
        }
        return false
    }

    private fun listenForMessages() {
        //riferimento dell'utente su Firebase
        val ref2 = FirebaseDatabase.getInstance().getReference("/users/"+FirebaseAuth.getInstance().uid)
        //listener per i dati
        ref2.addListenerForSingleValueEvent(object : ValueEventListener {

            override fun onDataChange(p0: DataSnapshot) {
                //salvo i dati nella variabile
                user2 = p0.getValue(User::class.java)
            }

            override fun onCancelled(p0: DatabaseError) {

            }
        })

        val fromId = FirebaseAuth.getInstance().uid
        val toId = toUser?.uid
        //riferimento dei messaggi inviati
        val ref = FirebaseDatabase.getInstance().getReference("/user_messages/$fromId/$toId")
        //listener per i figli
        ref.addChildEventListener(object: ChildEventListener {
            override fun onChildAdded(p0: DataSnapshot, p1: String1?) {
                //oggetto di tipo ChatMessage
                val chatMessage = p0.getValue(ChatMessage::class.java)
                if(chatMessage != null) {
                    //se fromid coincide con il mio id allora aggiungo all'adapter il mio messaggio
                    if (chatMessage.fromId == user2?.uid) {
                        if(chatMessage.type.equals("image")){
                            adapter.add(ChatFromItemImage(chatMessage.text, chatMessage.fromId, getDate(chatMessage.timeStamp)!!,getContext()!!))
                        }
                        else{
                            adapter.add(ChatFromItem(chatMessage.text, chatMessage.fromId, getDate(chatMessage.timeStamp)!!,getContext()!!))
                        }
                    } else {
                        //altrimenti aggiungo quello del mittente
                        if(chatMessage.type.equals("image")){
                            adapter.add(ChatToItemImage(chatMessage.text, chatMessage.fromId,getDate(chatMessage.timeStamp)!!,getContext()!!))
                        }else{
                            adapter.add(ChatToItem(chatMessage.text, chatMessage.fromId,getDate(chatMessage.timeStamp)!!,getContext()!!))
                        }
                    }
                 }
                recyclerview_chat_log.scrollToPosition(adapter.itemCount - 1)
            }

            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onChildChanged(p0: DataSnapshot, p1: String1?) {

            }

            override fun onChildMoved(p0: DataSnapshot, p1: String1?) {

            }

            override fun onChildRemoved(p0: DataSnapshot) {

            }
        })
    }

    private fun performSendMessage(type: String1) {

        //se il messaggio è un'immagine
        if(type.equals("image")){
            //se l'uri non è vuoto
            if(uri.isNotEmpty()){
                val text = uri
                val fromId = FirebaseAuth.getInstance().uid
                if (fromId == null) return
                val user = intent.getParcelableExtra<User>("USER_KEY")!!
                val toId = user.uid
                val key = FirebaseDatabase.getInstance().getReference("/user_messages/$fromId/$toId").push().key

                val reference = FirebaseDatabase.getInstance().getReference("/user_messages/$fromId/$toId/$key")

                val toReference = FirebaseDatabase.getInstance().getReference("/user_messages/$toId/$fromId/$key")

                val chatMessage = ChatMessage(
                    key!!,
                    text,
                    fromId,
                    toId,
                    System.currentTimeMillis() / 1000,
                    type
                )

                reference.setValue(chatMessage)
                toReference.setValue(chatMessage)
                val latestMessageRef = FirebaseDatabase.getInstance().getReference("latest-messages/$fromId/$toId")
                latestMessageRef.setValue(chatMessage)
                val latestMessageToRef = FirebaseDatabase.getInstance().getReference("latest-messages/$toId/$fromId")
                latestMessageToRef.setValue(chatMessage)
            }
        }else {
            //se il messaggio non è vuoto
            if (edittext_chat_log.text.toString().isNotEmpty()) {
                val text = edittext_chat_log.text.toString()
                val fromId = FirebaseAuth.getInstance().uid
                if (fromId == null) return
                val user :User = intent.getParcelableExtra<User>("USER_KEY")!!
                val toId : String1 = user.uid
                val key = FirebaseDatabase.getInstance().getReference("/user_messages/$fromId/$toId").push().key

                val reference = FirebaseDatabase.getInstance().getReference("/user_messages/$fromId/$toId/$key")

                val toReference = FirebaseDatabase.getInstance().getReference("/user_messages/$toId/$fromId/$key")

                val chatMessage = ChatMessage(
                    key!!,
                    text,
                    fromId,
                    toId,
                    System.currentTimeMillis() / 1000,
                    type
                )

                reference.setValue(chatMessage)
                toReference.setValue(chatMessage)
                val latestMessageRef = FirebaseDatabase.getInstance().getReference("latest-messages/$fromId/$toId")
                latestMessageRef.setValue(chatMessage)
                val latestMessageToRef = FirebaseDatabase.getInstance().getReference("latest-messages/$toId/$fromId")
                latestMessageToRef.setValue(chatMessage)
            }
        }
    }


    //metodo che recupera l'ora dell'inivio del messaggio
    @SuppressLint("SimpleDateFormat")
    fun getDate(timestamp: Long): String1? {
        try {
            val calendar: Calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"), Locale.getDefault())
            val tz: TimeZone = TimeZone.getDefault()
            calendar.timeInMillis = ((timestamp-7200) * 1000)
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.timeInMillis))
            val sdf = SimpleDateFormat("kk:mm:ss")
            val timeZone: Date = calendar.time as Date
            return sdf.format(timeZone)
        } catch (e: Exception) {
        }
        return ""
    }

    fun selectImage(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            when {
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE) -> {
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                }
                else -> {
                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(this,
                        arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                        MY_PERMISSION_READ_EXTERNAL_STORAGE
                    )

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            }
        } else {
            // Permission has already been granted
            val intent= Intent(Intent.ACTION_PICK)
            intent.type= "image/*"
            //apro selezionatore di immagini
            startActivityForResult(intent, 0)
        }
    }

    var selectedPhotoUri: Uri? =null
    var bitmap : Bitmap? =null
    //quando ho selezionato l'immagine
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        if(requestCode==0 && resultCode== Activity.RESULT_OK && data !=null){
            // controllo quale immagine è stata selezionata

            selectedPhotoUri=data.data


            //la salvo in una variabile
            bitmap= MediaStore.Images.Media.getBitmap(contentResolver,selectedPhotoUri)

            //ottengo l'orientamento originale dell'immagine

            val orientation = ExifInterface(this.getPath(selectedPhotoUri)!!).getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED
            )

            //ruoto l'immagine nella posizione corretta
            bitmap = ImageEditing.rotateBitmap(bitmap!!, orientation)

            //carico l'immagine su firebase
            uploadImageToFirebaseStorage()

        }
    }

    private fun uploadImageToFirebaseStorage(){
        var data: ByteArray?=null
        //carico immagine su firebase storage
        val filename= UUID.randomUUID().toString()
        val ref= FirebaseStorage.getInstance().getReference("/images/$filename")
        if(bitmap ==null){
            this.runOnUiThread {
                run {
                    val stream = ByteArrayOutputStream()
                    var bitmap: Bitmap =
                        BitmapFactory.decodeResource(resources, R.drawable.project)
                    bitmap = Bitmap.createScaledBitmap(bitmap, 200, 200, true)
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
                    data = stream.toByteArray()
                }}
        }else{
            this.runOnUiThread {
                run {
                    bitmap = Bitmap.createScaledBitmap(bitmap!!, 200, 200, true)
                    val baos = ByteArrayOutputStream()
                    bitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                    data = baos.toByteArray()
                }}
        }


        ref.putBytes(data!!)
            .addOnSuccessListener {
                ref.downloadUrl.addOnSuccessListener {
                    uri=it.toString()
                    performSendMessage("image")
                }
            }
            .addOnFailureListener {

            }
    }

    //ottengo la directory dell'immagine
    fun getPath(uri: Uri?): String1? {
        val projection = arrayOf(DATA)
        val cursor = uri?.let { contentResolver.query(it, projection, null, null, null) } ?: return null
        val column_index = cursor.getColumnIndexOrThrow(DATA)
        cursor.moveToFirst()
        val s = cursor.getString(column_index)
        cursor.close()
        return s
    }



    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String1>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSION_READ_EXTERNAL_STORAGE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //permessi ottenuti
                    val intent= Intent(Intent.ACTION_PICK)
                    intent.type= "image/*"
                    //apro selezionatore di immagini
                    startActivityForResult(intent,0)
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }


}








