package com.example.projectbook.messages

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.example.projectbook.models.ChatMessage
import com.example.projectbook.models.User
import com.example.projectbook.views.LatestMessageRow
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.example.projectbook.R

class MessagesFragment : Fragment() {

    companion object {
        private var mContext: Context? = null
        var currentUser: User? = null
        fun getContext(): Context? {
            return mContext
        }
    }

    private val adapter = GroupAdapter<GroupieViewHolder>()
    val latestMessagesMapNotSorted = LinkedHashMap<String, ChatMessage>()
    var latestMessagesMap: Map<String, ChatMessage>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
        mContext = context
        listenForLatestMessages()
        fetchCurrentUser()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.activity_latest_messages, container, false)
        val recyclerView = rootView.findViewById(R.id.recyclerview_latest_messages) as RecyclerView
        recyclerView.adapter = adapter
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                activity,
                DividerItemDecoration.VERTICAL
            )
        )
        adapter.setOnItemClickListener { item, _ ->
            val intent = Intent(activity, ChatLog::class.java)
            val row = item as LatestMessageRow
            if(row.chatPartnerUser!=null) {
                intent.putExtra("USER_KEY", row.chatPartnerUser)
                startActivity(intent)
            }
        }
        return rootView
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.messages_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_new_message -> {
                val intent = Intent(activity, NewMessage::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun refreshRecyclerViewMessages() {
        adapter.clear()
        latestMessagesMap!!.values.forEach {
            adapter.add(LatestMessageRow(it))
        }
    }

    private fun listenForLatestMessages() {
        val fromId = FirebaseAuth.getInstance().uid
        val ref = FirebaseDatabase.getInstance().getReference("/latest-messages/$fromId")
        ref.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                val chatMessage = p0.getValue(ChatMessage::class.java) ?: return
                latestMessagesMapNotSorted[p0.key!!] = chatMessage

                latestMessagesMap = latestMessagesMapNotSorted.toList()
                    .sortedByDescending { (_, value) -> value.timeStamp }
                    .toMap()

                refreshRecyclerViewMessages()
            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                val chatMessage = p0.getValue(ChatMessage::class.java) ?: return
                latestMessagesMapNotSorted[p0.key!!] = chatMessage
                latestMessagesMap = latestMessagesMapNotSorted.toList()
                    .sortedByDescending { (_, value) -> value.timeStamp }
                    .toMap()
                refreshRecyclerViewMessages()
            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
            }

            override fun onChildRemoved(p0: DataSnapshot) {
            }

            override fun onCancelled(p0: DatabaseError) {

            }
        })
    }


    private fun fetchCurrentUser() {
        val uid = FirebaseAuth.getInstance().uid
        val ref = FirebaseDatabase.getInstance().getReference("/users/$uid")
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {
                currentUser = p0.getValue(User::class.java)
            }
        })
    }
}
