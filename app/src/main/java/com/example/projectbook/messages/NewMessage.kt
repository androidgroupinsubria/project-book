package com.example.projectbook.messages

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.example.projectbook.R
import com.example.projectbook.models.User
import com.example.projectbook.views.UserItem
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.xwray.groupie.GroupAdapter
import kotlinx.android.synthetic.main.activity_new_message.*
import com.xwray.groupie.GroupieViewHolder

class NewMessage : AppCompatActivity() {
    private val adapter = GroupAdapter<GroupieViewHolder>()
    var userList: ArrayList<User> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_message)
        supportActionBar?.title = getString(R.string.sel_dest)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        fetchUser()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.new_messages_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.menu_mex_search -> {
                val searchView = item.actionView as androidx.appcompat.widget.SearchView
                searchView.queryHint = getString(R.string.cerca_email)
                searchView.setOnQueryTextListener(object :
                    androidx.appcompat.widget.SearchView.OnQueryTextListener {
                    override fun onQueryTextChange(newText: String): Boolean {
                        filterUsers(newText)
                        return false
                    }

                    override fun onQueryTextSubmit(query: String): Boolean {
                        return false
                    }
                })
            }

            android.R.id.home-> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun filterUsers(filter: String) {
        //ottengo il riferimento a project in Firebase
        val ref = FirebaseDatabase.getInstance().getReference("/users")
        //aggiungo un Listener
        ref.addValueEventListener(object : ValueEventListener {

            @SuppressLint("DefaultLocale")
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val data: ArrayList<User> = ArrayList()
                for (postSnapshot in dataSnapshot.children) {
                    val item: User? = postSnapshot.getValue(User::class.java)
                    if (item != null) {
                        //se il campo di ricerca è contenuto nei campi dell'oggetto
                        if (item.email.toUpperCase().contains(filter.toUpperCase()) && !item.uid.equals(
                                MessagesFragment.currentUser?.uid
                            )
                        )
                        //aggiungo l'oggetto alla lista
                            data.add(item)
                    }
                    userList.clear()
                    userList.addAll(data)
                    //aggiorno la schermata della ricerca
                    refreshUsersList()
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("SearchFragment", "loadPost:onCancelled", databaseError.toException())
            }
        })
    }

    private fun fetchUser() {
        val ref = FirebaseDatabase.getInstance().getReference("/users")
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                p0.children.forEach {
                    val user = it.getValue(User::class.java)
                    if (user != null && FirebaseAuth.getInstance().uid != user.uid)
                        adapter.add(UserItem(user.uid, false))
                }
                adapter.setOnItemClickListener { item, view ->
                    val userItem = item as UserItem
                    val intent = Intent(view.context, ChatLog::class.java)
                    intent.putExtra("USER_KEY", userItem.user)
                    startActivity(intent)
                    finish()
                }
                recyclerview_newmessage.adapter = adapter
            }

            override fun onCancelled(p0: DatabaseError) {

            }
        })
    }

    private fun refreshUsersList() {
        //pulisci l'adapter
        adapter.clear()
        //aggiungo gli oggetti all'adapter
        userList.forEach {
            adapter.add(UserItem(it.uid, false))
        }
    }
}



