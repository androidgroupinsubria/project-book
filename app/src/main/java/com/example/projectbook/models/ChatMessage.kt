package com.example.projectbook.models

class ChatMessage(val id:String, val text: String, val fromId: String, val toId: String, val timeStamp: Long,val type :String){
    constructor(): this("", "", "", "", -1,"")
}