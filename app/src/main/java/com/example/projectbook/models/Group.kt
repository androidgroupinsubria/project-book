package com.example.projectbook.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Group(val id: String, val title:String, val authorId:String, val authorNickname:String, val groupImage:String) : Parcelable{
    constructor() : this("","", "", "", "")
}