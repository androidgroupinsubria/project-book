package com.example.projectbook.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Project(val authorId: String, val authorNickname : String, val title: String, val desc: String,val timeStamp: Long,val imageproject:String): Parcelable{
    constructor(): this("", "", "", "", -1,"")
}