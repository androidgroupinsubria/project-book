package com.example.projectbook.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class User(val uid:String,val username: String,val profileImageUrl:String,val job:String, val bio: String, val place:String, val site : String, val email: String, val linkedin: String, val twitter: String): Parcelable {
    constructor() : this("", "", "","","", "","", "", "", "")
}