package com.example.projectbook.profile

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.example.projectbook.messages.ChatLog
import com.example.projectbook.models.Project
import com.example.projectbook.models.User
import com.example.projectbook.project.ShowProject
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.activity_profile.*
import java.util.*
import com.example.projectbook.R
import com.example.projectbook.views.ProjectRow

//id dell'utente su Firebase
var uId: String? = null

//classe che mostra il profilo di un utente
class Profile : AppCompatActivity() {

    private var toUser: User? = null
    private val adapter = GroupAdapter<GroupieViewHolder>()

    //Mappa che contiene i progetti
    val projectHashMap = LinkedHashMap<String, Project>()
    var projectMap: Map<String, Project>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        //dati utente
        toUser = intent.getParcelableExtra("USER_KEY")
        uId = toUser!!.uid
        val recyclerView = this.findViewById(R.id.recyclerview_myproject_profile) as RecyclerView
        //listener sui progetti
        adapter.setOnItemClickListener { item, _ ->
            val intent = Intent(this, ShowProject::class.java)
            val row = item as ProjectRow
            val bundle = Bundle()
            bundle.putParcelable("PROJECT", row.project)
            bundle.putString("KEY", row.key)
            intent.putExtras(bundle)
            startActivity(intent)
        }
        recyclerView.adapter = adapter
        recyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        recyclerView.isNestedScrollingEnabled = false
        showMyProjects()
        mod_photo_project.text = toUser!!.username
        Picasso.get().load(toUser!!.profileImageUrl).placeholder(R.drawable.progress_animation)
            .into(photo_profile)
        aProfileJob.text = toUser!!.job
        aProfileAbout.text = toUser!!.bio
        //listener su icona mappa che apre la mappa di google e calcola il percorso migliore in base alla posizione dell'user
        aProfileWhere.setOnClickListener {
            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("google.navigation:q=" + toUser!!.place)
            )
            startActivity(intent)
        }

        aProfileMail.text = toUser!!.email
        aProfileWebsite.text = toUser!!.site

        //listener su sito
        if (toUser!!.site!= "") {
            aProfileWebsite.setOnClickListener {
                val intent = Intent()
                intent.action = Intent.ACTION_VIEW
                intent.addCategory(Intent.CATEGORY_BROWSABLE)
                intent.data = Uri.parse(toUser!!.site)
                startActivity(intent)
            }
        }

        //listener su icona linkedin
        if (toUser!!.linkedin != "") {
            linkedin_link.setOnClickListener {
                val intent = Intent()
                intent.action = Intent.ACTION_VIEW
                intent.addCategory(Intent.CATEGORY_BROWSABLE)
                intent.data = Uri.parse(toUser!!.linkedin)
                startActivity(intent)
            }
        }
        //listener su icona twitter
        if (toUser!!.twitter != "") {
            twitter_link.setOnClickListener {
                val intent = Intent()
                intent.action = Intent.ACTION_VIEW
                intent.addCategory(Intent.CATEGORY_BROWSABLE)
                intent.data = Uri.parse(toUser!!.twitter)
                startActivity(intent)
            }
        }
    }


    override fun onResume() {
        super.onResume()
        adapter.clear()
        showMyProjects()
        toUser = intent.getParcelableExtra("USER_KEY")
        uId = toUser!!.uid

        val ref = FirebaseDatabase.getInstance().getReference("users").child(toUser!!.uid)
        val listener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val user = dataSnapshot.getValue(User::class.java)
                mod_photo_project.text = user!!.username
                Picasso.get().load(user.profileImageUrl).placeholder(R.drawable.progress_animation)
                    .into(photo_profile)
                aProfileJob.text = user.job
                aProfileAbout.text = user.bio
                aProfileWhere.setOnClickListener {
                    val intent = Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("google.navigation:q=" + user.place)
                    )
                    startActivity(intent)
                }
                aProfileMail.text = user.email
                aProfileWebsite.text = user.site

                if (user.site != "") {
                    aProfileWebsite.setOnClickListener {
                        val intent = Intent()
                        intent.action = Intent.ACTION_VIEW
                        intent.addCategory(Intent.CATEGORY_BROWSABLE)
                        intent.data = Uri.parse(user.site)
                        startActivity(intent)
                    }
                }

                if (user.linkedin != "") {
                    linkedin_link.setOnClickListener {
                        val intent = Intent()
                        intent.action = Intent.ACTION_VIEW
                        intent.addCategory(Intent.CATEGORY_BROWSABLE)
                        intent.data = Uri.parse(user.linkedin)
                        startActivity(intent)
                    }
                }

                if (user.twitter != "") {
                    twitter_link.setOnClickListener {
                        val intent = Intent()
                        intent.action = Intent.ACTION_VIEW
                        intent.addCategory(Intent.CATEGORY_BROWSABLE)
                        intent.data = Uri.parse(user.twitter)
                        startActivity(intent)
                    }
                }

            }

            override fun onCancelled(databaseError: DatabaseError) {
                // handle error
            }
        }
        ref.addListenerForSingleValueEvent(listener)


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        //se è il mio profilo allora mostro nel menu la possibilità di modificarlo
        if (FirebaseAuth.getInstance().currentUser!!.uid == uId) {
            val inflater = menuInflater
            inflater.inflate(R.menu.profile_menu, menu)
        } else {
            //altrimenti mostro l'icona per chattare con l'utente
            val inflater = menuInflater
            inflater.inflate(R.menu.profile_menu_chat, menu)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        //se si clicca indietro termina l'activity
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
            }
            //se si clicca su impostazioni si rimanda l'user all'activity delle impostazioni
            R.id.menu_profile_settings -> {
                //creo un intent sulla classe ProfileSettings e passo i dati dell'user
                val intent = Intent(this, ProfileSettings::class.java)
                intent.putExtra("USER_KEY", toUser)
                startActivity(intent)
                //termino l'activity
                //finish()
            }
            //se si clicca sul'icona dei messaggi si rimanda alla chat con l'user
            R.id.profile_chat -> {
                val intent = Intent(this, ChatLog::class.java)
                intent.putExtra("USER_KEY", toUser)
                startActivity(intent)
            }
        }

        @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
        return super.onOptionsItemSelected(item)
    }

    //metodo che mostra i progetti dell'user
    private fun showMyProjects() {
        projectHashMap.clear()
        val ref =
            FirebaseDatabase.getInstance().reference.child("projects").orderByChild("authorId")
                .equalTo(uId)
        ref.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                val newProject = p0.getValue(Project::class.java) ?: return
                projectHashMap[p0.key!!] = newProject
                projectMap = projectHashMap.toList()
                    .sortedByDescending { (_, value) -> value.timeStamp }
                    .toMap()
                refreshRecyclerViewProjects()
            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                val newProject = p0.getValue(Project::class.java) ?: return
                projectHashMap[p0.key!!] = newProject
                projectMap = projectHashMap.toList()
                    .sortedByDescending { (_, value) -> value.timeStamp }
                    .toMap()
                refreshRecyclerViewProjects()
            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
            }

            override fun onChildRemoved(p0: DataSnapshot) {
                val newProject = p0.getValue(Project::class.java) ?: return
                projectHashMap[p0.key!!] = newProject
                projectMap = projectHashMap.toList()
                    .sortedByDescending { (_, value) -> value.timeStamp }
                    .toMap()
                refreshRecyclerViewProjects()
            }

            override fun onCancelled(p0: DatabaseError) {

            }
        })
    }

    private fun refreshRecyclerViewProjects() {
        if (projectMap != null) {
            adapter.clear()
            for ((key, value) in projectMap!!) {
                adapter.add(ProjectRow(value, key))
            }
        }
    }

}




