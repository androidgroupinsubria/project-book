package com.example.projectbook.profile

import android.annotation.SuppressLint
import android.app.Activity
import com.example.projectbook.R
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.Selection
import android.text.TextWatcher
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import com.example.projectbook.database.DatabaseHelper
import com.example.projectbook.models.User
import com.example.projectbook.registerlogin.MY_PERMISSION_READ_EXTERNAL_STORAGE
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_profile_settings.*
import java.io.ByteArrayOutputStream
import java.util.*


class ProfileSettings : AppCompatActivity() {

    var toUser: User? = null
    var changed: Boolean = false

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_settings)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        //recupero i dati dell'user passati dall'intent e li inserisco nelle view
        toUser = intent.getParcelableExtra<User>("USER_KEY")
        ProfileSettingsTitle.setText(toUser!!.username)
        Picasso.get().load(toUser!!.profileImageUrl).into(image_profile_settings)
        ProfileSettingsTitle.setText(toUser!!.username)
        ProfileSettingsWork.setText(toUser!!.job)
        ProfileSettingsBio.setText(toUser!!.bio)
        ProfileSettingsLocation.setText(toUser!!.place)
        //aggiungo prefisso alla editext
        ProfileSettingsWebsite.setText("http://")
        Selection.setSelection(ProfileSettingsWebsite.text, ProfileSettingsWebsite.text.length)
        ProfileSettingsWebsite.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun afterTextChanged(s: Editable) {
                if (!s.toString().startsWith("http://")) {
                    ProfileSettingsWebsite.setText("http://")
                    Selection.setSelection(
                        ProfileSettingsWebsite.text,
                        ProfileSettingsWebsite.text.length
                    )

                }
            }
        })
        ProfileSettingsWebsite.setText(toUser!!.site)
        ProfileSettingsEmail.setText(toUser!!.email)
        ProfileSettingsEmail.isEnabled = false
        //aggiungo prefisso alla editext
        ProfileSettingsLinkedin.setText("http://")
        Selection.setSelection(ProfileSettingsLinkedin.text, ProfileSettingsLinkedin.text.length)
        ProfileSettingsLinkedin.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun afterTextChanged(s: Editable) {
                if (!s.toString().startsWith("http://")) {
                    ProfileSettingsLinkedin.setText("http://")
                    Selection.setSelection(
                        ProfileSettingsLinkedin.text,
                        ProfileSettingsLinkedin.text.length
                    )

                }
            }
        })
        ProfileSettingsLinkedin.setText(toUser!!.linkedin)
        //aggiungo prefisso alla editext
        ProfileSettingsTwitter.setText("http://")
        Selection.setSelection(ProfileSettingsTwitter.text, ProfileSettingsTwitter.text.length)
        ProfileSettingsTwitter.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun afterTextChanged(s: Editable) {
                if (!s.toString().startsWith("http://")) {
                    ProfileSettingsTwitter.setText("http://")
                    Selection.setSelection(
                        ProfileSettingsTwitter.text,
                        ProfileSettingsTwitter.text.length
                    )

                }
            }
        })
        ProfileSettingsTwitter.setText(toUser!!.twitter)

        //listener per il caricamento di un'immagine profilo
        image_profile_settings.setOnClickListener {

            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(
                    this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                )
                != PackageManager.PERMISSION_GRANTED
            ) {

                // Permission is not granted
                // Should we show an explanation?
                when {
                    ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE
                    ) -> {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
                    }
                    else -> {
                        // No explanation needed, we can request the permission.

                        ActivityCompat.requestPermissions(
                            this,
                            arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                            MY_PERMISSION_READ_EXTERNAL_STORAGE
                        )

                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                }
            } else {
                // Permission has already been granted
                Log.d("MainActivity", "apro la galleria")
                val intent = Intent(Intent.ACTION_PICK)
                intent.type = "image/*"
                //apro selezionatore di immagini
                startActivityForResult(intent, 0)
            }


        }

        //listener per la modifica dell'immagine profilo
        mod_photo_profile.setOnClickListener {

            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(
                    this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                )
                != PackageManager.PERMISSION_GRANTED
            ) {

                // Permission is not granted
                // Should we show an explanation?
                when {
                    ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE
                    ) -> {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
                    }
                    else -> {
                        // No explanation needed, we can request the permission.

                        ActivityCompat.requestPermissions(
                            this,
                            arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                            MY_PERMISSION_READ_EXTERNAL_STORAGE
                        )

                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                }
            } else {
                // Permission has already been granted
                Log.d("MainActivity", "apro la galleria")
                val intent = Intent(Intent.ACTION_PICK)
                intent.type = "image/*"
                //apro selezionatore di immagini
                startActivityForResult(intent, 0)
            }


        }

    }

    //onResume
    override fun onResume() {
        super.onResume()
        //recupero i dati dell'utente
        toUser = intent.getParcelableExtra<User>("USER_KEY")
        //recupero l'id dell'utente
        uId = toUser!!.uid
        //recupero il riferimento su Firebase
        val ref = FirebaseDatabase.getInstance().getReference("users").child(toUser!!.uid)
        //listener
        val Listener = object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                //recupero i dati dell'utente e li inserisco nelle view
                val user = dataSnapshot.getValue(User::class.java)
                ProfileSettingsTitle.setText(user!!.username)
                ProfileSettingsTitle.setText(user.username)
                ProfileSettingsWork.setText(user.job)
                ProfileSettingsBio.setText(user.bio)
                ProfileSettingsLocation.setText(user.place)
                ProfileSettingsWebsite.setText("http://")
                Selection.setSelection(
                    ProfileSettingsWebsite.text,
                    ProfileSettingsWebsite.text.length
                )
                ProfileSettingsWebsite.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(
                        charSequence: CharSequence,
                        i: Int,
                        i1: Int,
                        i2: Int
                    ) {
                    }

                    override fun onTextChanged(
                        charSequence: CharSequence,
                        i: Int,
                        i1: Int,
                        i2: Int
                    ) {
                    }

                    override fun afterTextChanged(s: Editable) {
                        if (!s.toString().startsWith("http://")) {
                            ProfileSettingsWebsite.setText("http://")
                            Selection.setSelection(
                                ProfileSettingsWebsite.text,
                                ProfileSettingsWebsite.text.length
                            )

                        }
                    }
                })
                ProfileSettingsWebsite.setText(user.site)
                ProfileSettingsEmail.setText(user.email)
                ProfileSettingsEmail.isEnabled = false
                //aggiungo prefisso alla editext
                ProfileSettingsLinkedin.setText("http://")
                Selection.setSelection(
                    ProfileSettingsLinkedin.text,
                    ProfileSettingsLinkedin.text.length
                )
                ProfileSettingsLinkedin.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(
                        charSequence: CharSequence,
                        i: Int,
                        i1: Int,
                        i2: Int
                    ) {
                    }

                    override fun onTextChanged(
                        charSequence: CharSequence,
                        i: Int,
                        i1: Int,
                        i2: Int
                    ) {
                    }

                    override fun afterTextChanged(s: Editable) {
                        if (!s.toString().startsWith("http://")) {
                            ProfileSettingsLinkedin.setText("http://")
                            Selection.setSelection(
                                ProfileSettingsLinkedin.text,
                                ProfileSettingsLinkedin.text.length
                            )

                        }
                    }
                })
                ProfileSettingsLinkedin.setText(user.linkedin)
                //aggiungo prefisso alla editext
                ProfileSettingsTwitter.setText("http://")
                Selection.setSelection(
                    ProfileSettingsTwitter.text,
                    ProfileSettingsTwitter.text.length
                )
                ProfileSettingsTwitter.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(
                        charSequence: CharSequence,
                        i: Int,
                        i1: Int,
                        i2: Int
                    ) {
                    }

                    override fun onTextChanged(
                        charSequence: CharSequence,
                        i: Int,
                        i1: Int,
                        i2: Int
                    ) {
                    }

                    override fun afterTextChanged(s: Editable) {
                        if (!s.toString().startsWith("http://")) {
                            ProfileSettingsTwitter.setText("http://")
                            Selection.setSelection(
                                ProfileSettingsTwitter.text,
                                ProfileSettingsTwitter.text.length
                            )

                        }
                    }
                })
                ProfileSettingsTwitter.setText(user.twitter)

            }

            override fun onCancelled(databaseError: DatabaseError) {
                // handle error
            }
        }
        ref.addListenerForSingleValueEvent(Listener)


    }

    var selectedPhotoUri: Uri? = null
    var bitmap: Bitmap? = null
    //quando ho selezionato l'immagine
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            // controllo quale immagine è stata selezionata
            selectedPhotoUri = data.data
            //la salvo in una variabile
            @Suppress("DEPRECATION")
            bitmap = MediaStore.Images.Media.getBitmap(contentResolver, selectedPhotoUri!!)
            //ottengo l'orientamento originale dell'immagine
            val orientation = ExifInterface(getPath(selectedPhotoUri)!!).getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED
            )
            //ruoto l'immagine nella posizione corretta
            bitmap = rotateBitmap(bitmap!!, orientation)
            Log.d("ciao", "ciao")
            image_profile_settings.setImageBitmap(bitmap)
            //l'immagine è stata cambiata
            changed = true

        }
    }


    fun rotateBitmap(bitmap: Bitmap, orientation: Int): Bitmap? {
        val matrix = Matrix()
        when (orientation) {
            ExifInterface.ORIENTATION_NORMAL -> return bitmap
            ExifInterface.ORIENTATION_FLIP_HORIZONTAL -> matrix.setScale(-1f, 1f)
            ExifInterface.ORIENTATION_ROTATE_180 -> matrix.setRotate(180f)
            ExifInterface.ORIENTATION_FLIP_VERTICAL -> {
                matrix.setRotate(180f)
                matrix.postScale(-1f, 1f)
            }
            ExifInterface.ORIENTATION_TRANSPOSE -> {
                matrix.setRotate(90f)
                matrix.postScale(-1f, 1f)
            }
            ExifInterface.ORIENTATION_ROTATE_90 -> matrix.setRotate(90f)
            ExifInterface.ORIENTATION_TRANSVERSE -> {
                matrix.setRotate(-90f)
                matrix.postScale(-1f, 1f)
            }
            ExifInterface.ORIENTATION_ROTATE_270 -> matrix.setRotate(-90f)
            else -> return bitmap
        }
        return try {
            val bmRotated = Bitmap.createBitmap(
                bitmap,
                0,
                0,
                bitmap.width,
                bitmap.height,
                matrix,
                true
            )
            bitmap.recycle()
            bmRotated
        } catch (e: OutOfMemoryError) {
            e.printStackTrace()
            null
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            MY_PERMISSION_READ_EXTERNAL_STORAGE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //permessi ottenuti
                    Log.d("MainActivity", "apro la galleria")
                    val intent = Intent(Intent.ACTION_PICK)
                    intent.type = "image/*"
                    //apro selezionatore di immagini
                    startActivityForResult(intent, 0)
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }

    //recupero il percorso dell'immagine nel dispositivo
    private fun getPath(uri: Uri?): String? {
        @Suppress("DEPRECATION") val projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor =
            uri?.let { contentResolver.query(it, projection, null, null, null) } ?: return null
        @Suppress("DEPRECATION") val columnIndex =
            cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        val s = cursor.getString(columnIndex)
        cursor.close()
        return s
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.profile_settings_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }

        when (item.itemId) {
            R.id.menu_profile_settings_save -> {
                if (changed == true) {
                    uploadImageToFirebaseStorage()
                } else {
                    saveUserToFirebaseDatabase(toUser!!.profileImageUrl)
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun uploadImageToFirebaseStorage() {
        val data: ByteArray
        //carico immagine su firebase storage
        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")
        if (bitmap == null) {
            val stream = ByteArrayOutputStream()
            var bitmap = AppCompatResources.getDrawable(this, R.drawable.ic_user)!!.toBitmap()
            bitmap = Bitmap.createScaledBitmap(bitmap, 100, 150, true)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
            data = stream.toByteArray()
        } else {
            bitmap = Bitmap.createScaledBitmap(bitmap!!, 150, 150, true)
            val baos = ByteArrayOutputStream()
            bitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            data = baos.toByteArray()
        }


        ref.putBytes(data)
            .addOnSuccessListener {
                Log.d("RegisterActivity", "Immagine caricata con successo :${it.metadata?.path}")

                ref.downloadUrl.addOnSuccessListener {
                    it.toString()
                    Log.d("RegisterActivity", "file Location: " + it)
                    saveUserToFirebaseDatabase(it.toString())
                }
            }
            .addOnFailureListener {

            }
    }

    private fun saveUserToFirebaseDatabase(profileImageUrl: String) {
        val uid = FirebaseAuth.getInstance().uid ?: ""
        val username = ProfileSettingsTitle.text.toString()
        val ref = FirebaseDatabase.getInstance().getReference("/users/$uid")
        val job = ProfileSettingsWork.text.toString()
        val bio = ProfileSettingsBio.text.toString()
        val place = ProfileSettingsLocation.text.toString()
        val site = ProfileSettingsWebsite.text.toString()
        val email = ProfileSettingsEmail.text.toString()
        val linkedin = ProfileSettingsLinkedin.text.toString()
        val twitter = ProfileSettingsTwitter.text.toString()
        val user = User(
            uid,
            username,
            profileImageUrl,
            job,
            bio,
            place,
            site,
            email,
            linkedin,
            twitter
        )
        ref.setValue(user)
            .addOnSuccessListener {
                val helper = DatabaseHelper(this)
                //aggiorno dati anche nel db locale
                if (helper.updateUserSettings(uid, email, username, profileImageUrl) > 0) {
                    finish()
                }
            }
    }

    override fun onBackPressed() {
        finish()
    }
}
