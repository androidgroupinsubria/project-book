package com.example.projectbook.project

import android.app.Activity
import com.example.projectbook.R
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.projectbook.ImageEditing
import com.example.projectbook.models.Project
import com.example.projectbook.models.User
import com.example.projectbook.registerlogin.MY_PERMISSION_READ_EXTERNAL_STORAGE
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.add_new_project.*
import java.io.ByteArrayOutputStream
import java.util.*


class Addproject : AppCompatActivity() {
    var user: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_new_project)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        user = intent.getParcelableExtra<User>("USER_KEY")

        image_project_settings.setOnClickListener {

            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(
                    this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                )
                != PackageManager.PERMISSION_GRANTED
            ) {

                // Permission is not granted
                // Should we show an explanation?
                when {
                    ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE
                    ) -> {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
                    }
                    else -> {
                        // No explanation needed, we can request the permission.

                        ActivityCompat.requestPermissions(
                            this,
                            arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                            MY_PERMISSION_READ_EXTERNAL_STORAGE
                        )

                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                }
            } else {
                // Permission has already been granted
                val intent = Intent(Intent.ACTION_PICK)
                intent.type = "image/*"
                //apro selezionatore di immagini
                startActivityForResult(intent, 0)
            }


        }

        mod_photo_project.setOnClickListener {

            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(
                    this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                )
                != PackageManager.PERMISSION_GRANTED
            ) {

                // Permission is not granted
                // Should we show an explanation?
                when {
                    ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE
                    ) -> {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
                    }
                    else -> {
                        // No explanation needed, we can request the permission.

                        ActivityCompat.requestPermissions(
                            this,
                            arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                            MY_PERMISSION_READ_EXTERNAL_STORAGE
                        )

                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                }
            } else {
                // Permission has already been granted
                val intent = Intent(Intent.ACTION_PICK)
                intent.type = "image/*"
                //apro selezionatore di immagini
                startActivityForResult(intent, 0)
            }


        }

    }


    var selectedPhotoUri: Uri? = null
    var bitmap: Bitmap? = null
    //quando ho selezionato l'immagine
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            // controllo quale immagine è stata selezionata

            selectedPhotoUri = data.data


            //la salvo in una variabile
            @Suppress("DEPRECATION")
            bitmap = MediaStore.Images.Media.getBitmap(contentResolver, selectedPhotoUri)

            //ottengo l'orientamento originale dell'immagine

            val orientation = ExifInterface(getPath(selectedPhotoUri)!!).getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED
            )

            //ruoto l'immagine nella posizione corretta
            bitmap = ImageEditing.rotateBitmap(bitmap!!, orientation)

            image_project_settings.setImageBitmap(bitmap)

            //image_profile_settings.alpha = 0f


        }
    }

    //ottengo la directory dell'immagine
    fun getPath(uri: Uri?): String? {
        @Suppress("DEPRECATION") val projection =
            arrayOf(MediaStore.Images.Media.DATA)
        val cursor =
            uri?.let { contentResolver.query(it, projection, null, null, null) } ?: return null
        val columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        val s = cursor.getString(columnIndex)
        cursor.close()
        return s
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            MY_PERMISSION_READ_EXTERNAL_STORAGE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //permessi ottenuti
                    val intent = Intent(Intent.ACTION_PICK)
                    intent.type = "image/*"
                    //apro selezionatore di immagini
                    startActivityForResult(intent, 0)
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.project_settings_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }

        when (item.itemId) {


            R.id.menu_project_settings_save -> {
                if (ProjectSettingsTitle.text.toString().equals("") || ProjectSettingsDesc.text.toString().equals(
                        ""
                    )
                ) {
                    Toast.makeText(this, "Completa i campi", Toast.LENGTH_SHORT).show()
                } else {
                    uploadImageToFirebaseStorage()
                }
            }
        }


        return super.onOptionsItemSelected(item)
    }

    private fun uploadImageToFirebaseStorage() {
        var data: ByteArray? = null
        //carico immagine su firebase storage
        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")
        if (bitmap == null) {
            this.runOnUiThread {
                run {
                    val stream = ByteArrayOutputStream()
                    var bitmap: Bitmap =
                        BitmapFactory.decodeResource(resources, R.drawable.project)
                    bitmap = Bitmap.createScaledBitmap(bitmap, 200, 200, true)
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
                    data = stream.toByteArray()
                }
            }
        } else {
            this.runOnUiThread {
                run {
                    bitmap = Bitmap.createScaledBitmap(bitmap!!, 200, 200, true)
                    val baos = ByteArrayOutputStream()
                    bitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                    data = baos.toByteArray()
                }
            }
        }


        ref.putBytes(data!!)
            .addOnSuccessListener {
                ref.downloadUrl.addOnSuccessListener {
                    saveProjectToFirebaseDatabase(it.toString())
                }
            }
            .addOnFailureListener {

            }
    }

    private fun saveProjectToFirebaseDatabase(profileImageUrl: String) {
        val authorId = FirebaseAuth.getInstance().uid ?: ""
        val authorNick = user!!.username
        val title = ProjectSettingsTitle.text.toString()
        val desc = ProjectSettingsDesc.text.toString()
        val ref = FirebaseDatabase.getInstance().getReference("/projects/").push()
        val project = Project(
            authorId,
            authorNick,
            title,
            desc,
            System.currentTimeMillis() / 1000,
            profileImageUrl
        )
        ref.setValue(project)
            .addOnSuccessListener {
                finish()
            }
    }

    override fun onBackPressed() {
        finish()
    }
}
