package com.example.projectbook.project

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.example.projectbook.models.Project
import com.example.projectbook.views.ProjectRow
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.example.projectbook.R

class Myproject : AppCompatActivity() {

    private val adapter = GroupAdapter<GroupieViewHolder>()
    val projectHashMap = LinkedHashMap<String, Project>()
    var projectMap: Map<String, Project>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.my_project)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val recyclerView = this.findViewById(R.id.recyclerview_myproject) as RecyclerView
        adapter.setOnItemClickListener { item, _ ->
            val intent = Intent(this, ShowProject::class.java)
            val row = item as ProjectRow
            val bundle = Bundle()
            bundle.putParcelable("PROJECT", row.project)
            bundle.putString("KEY", row.key)
            intent.putExtras(bundle)
            startActivity(intent)
        }
        recyclerView.adapter = adapter
        recyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        showMyProjects()
    }

    override fun onResume() {
        super.onResume()
        adapter.clear()
        showMyProjects()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showMyProjects() {
        projectHashMap.clear()
        val ref = FirebaseDatabase.getInstance().reference.child("projects").orderByChild("authorId")
                .equalTo(FirebaseAuth.getInstance().uid)
        ref.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                val newProject = p0.getValue(Project::class.java) ?: return
                projectHashMap[p0.key!!] = newProject
                projectMap = projectHashMap.toList()
                    .sortedByDescending { (_, value) -> value.timeStamp }
                    .toMap()
                refreshRecyclerViewProjects()
            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                val newProject = p0.getValue(Project::class.java) ?: return
                projectHashMap[p0.key!!] = newProject
                projectMap = projectHashMap.toList()
                    .sortedByDescending { (_, value) -> value.timeStamp }
                    .toMap()
                refreshRecyclerViewProjects()
            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
            }

            override fun onChildRemoved(p0: DataSnapshot) {
                val newProject = p0.getValue(Project::class.java) ?: return
                projectHashMap[p0.key!!] = newProject
                projectMap = projectHashMap.toList()
                    .sortedByDescending { (_, value) -> value.timeStamp }
                    .toMap()
                refreshRecyclerViewProjects()
            }

            override fun onCancelled(p0: DatabaseError) {

            }
        })
    }

    private fun refreshRecyclerViewProjects() {
        if (projectMap != null) {
            adapter.clear()
            for ((key, value) in projectMap!!) {
                adapter.add(ProjectRow(value, key))
            }
        }
    }


    override fun onBackPressed() {
        finish()
    }
}