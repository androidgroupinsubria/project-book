package com.example.projectbook.project

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.format.DateFormat
import android.transition.TransitionManager
import android.view.MenuItem
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import com.example.projectbook.R
import com.example.projectbook.messages.ChatLog
import com.example.projectbook.models.Project
import com.example.projectbook.models.User
import com.example.projectbook.profile.Profile
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_show_project.*
import java.util.*

class ShowProject : AppCompatActivity() {

    companion object UserData {
        var user: User? = null
    }

    private var isOpen: Boolean = false
    private var layoutOne: ConstraintSet? = null
    private var layoutTwo: ConstraintSet? = null
    private var constraintLayout: ConstraintLayout? = null

    var project: Project? = null
    var key: String = ""

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_project)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        layoutOne = ConstraintSet()
        layoutTwo = ConstraintSet()
        constraintLayout = project_layout
        layoutTwo?.clone(this, R.layout.activity_show_project_expanded)
        layoutOne?.clone(constraintLayout)
        project_imageview.setOnClickListener {
            isOpen = if (!isOpen) {
                TransitionManager.beginDelayedTransition(constraintLayout)
                layoutTwo?.applyTo(constraintLayout)
                !isOpen
            } else {
                TransitionManager.beginDelayedTransition(constraintLayout)
                layoutOne?.applyTo(constraintLayout)
                !isOpen
            }
        }

        project = intent.getParcelableExtra("PROJECT")
        key = intent.getStringExtra("KEY")!!

        Picasso.get().load(project!!.imageproject).placeholder(R.drawable.progress_animation)
            .into(project_imageview)
        supportActionBar!!.title = project!!.title
        author_textview.text = "Autore: " + project!!.authorNickname
        chiedi_di_partecipare.text =
            chiedi_di_partecipare.text.toString() + " " + project!!.authorNickname

        val ref2 = FirebaseDatabase.getInstance().getReference("/users/" + project!!.authorId)
        //listener per i dati
        ref2.addListenerForSingleValueEvent(object : ValueEventListener {

            override fun onDataChange(p0: DataSnapshot) {
                //salvo i dati nella variabile
                user = p0.getValue(User::class.java)
            }

            override fun onCancelled(p0: DatabaseError) {

            }
        })

        author_textview.setOnClickListener {
            val intent = Intent(this, Profile::class.java)
            intent.putExtra("USER_KEY", user)
            startActivity(intent)
        }
        time_textview.text = getDate(project!!.timeStamp)
        desc_textview.text = project!!.desc

        if (project!!.authorId.equals(FirebaseAuth.getInstance().uid)) {
            chiedi_di_partecipare.text = getString(R.string.elimina)
        }

        //listener per chiede di partecipare ad un progetto
        chiedi_di_partecipare.setOnClickListener {
            if (project!!.authorId.equals(FirebaseAuth.getInstance().uid)) {
                val ref3 = FirebaseDatabase.getInstance()
                ref3.getReference("projects")
                    .child(key)
                    .removeValue()

                finish()
            } else {
                val intent = Intent(this, ChatLog::class.java)
                intent.putExtra("USER_KEY", user)
                startActivity(intent)
            }
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getDate(timestamp: Long): String {
        val calendar = Calendar.getInstance(Locale.ENGLISH)
        calendar.timeInMillis = timestamp * 1000L
        val date = DateFormat.format("dd-MM-yyyy", calendar).toString()
        return date
    }
}
