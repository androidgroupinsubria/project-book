package com.example.projectbook.registerlogin

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.projectbook.main.MainActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*
import com.example.projectbook.R

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        //nascondo l'ActionBar
        supportActionBar!!.hide()

        login_button_login.setOnClickListener {

            val email = email_edittext_login.text.toString()
            val password = password_edittext_login.text.toString()

            if (email.isNotEmpty() && password.isNotEmpty()) {
                FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener {
                        if (it.isSuccessful) {
                            val intent = Intent(this, MainActivity::class.java)
                            startActivity(intent)
                        } else {
                            //pulisco le edittext
                            email_edittext_login.setText("")
                            password_edittext_login.setText("")
                            //stampo messaggio di errore
                            Toast.makeText(
                                this,
                                (it.exception?.message),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            }else{
                Toast.makeText(
                    this,
                    "Inserire username o password",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        back_to_register_textview.setOnClickListener {
            //avvio RegisterActivity
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
        log_back.setOnClickListener {
            //torno indietro
            onBackPressed()
        }
    }
}