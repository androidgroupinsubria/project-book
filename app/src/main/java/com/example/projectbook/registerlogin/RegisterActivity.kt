package com.example.projectbook.registerlogin

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import com.example.projectbook.ImageEditing
import com.example.projectbook.R
import com.example.projectbook.database.DatabaseHelper
import com.example.projectbook.main.MainActivity
import com.example.projectbook.models.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_register.*
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*


const val MY_PERMISSION_READ_EXTERNAL_STORAGE = 101

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        //nascondo l'ActionBar
        supportActionBar!!.hide()
        //se l'utente clicca sul tasto registrati
        register_button_register.setOnClickListener {
            performRegister()
        }
        //se l'utente torna indietro
        reg_back.setOnClickListener {
            onBackPressed()
        }
        //se l'utente ha già un account
        alredy_have_account_text_view.setOnClickListener {

            //avvio login_activity
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)

        }
        //se l'utente scegli di selezionare una foto
        selectphoto_button_register.setOnClickListener {

            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(
                    this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                )
                != PackageManager.PERMISSION_GRANTED
            ) {

                // Permission is not granted
                // Should we show an explanation?
                when {
                    ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE
                    ) -> {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
                    }
                    else -> {
                        // No explanation needed, we can request the permission.

                        ActivityCompat.requestPermissions(
                            this,
                            arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                            MY_PERMISSION_READ_EXTERNAL_STORAGE
                        )

                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                }
            } else {
                // Permission has already been granted
                Log.d("MainActivity", "apro la galleria")
                val intent = Intent(Intent.ACTION_PICK)
                intent.type = "image/*"
                //apro selezionatore di immagini
                startActivityForResult(intent, 0)
            }


        }
    }

    var selectedPhotoUri: Uri? = null
    var bitmap: Bitmap? = null
    //quando ho selezionato l'immagine
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            // controllo quale immagine è stata selezionata

            selectedPhotoUri = data.data


            //la salvo in una variabile
            @Suppress("DEPRECATION")
            bitmap = MediaStore.Images.Media.getBitmap(contentResolver, selectedPhotoUri)

            //ottengo l'orientamento originale dell'immagine

            val orientation = ExifInterface(getPath(selectedPhotoUri)!!).getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED
            )


            //ruoto l'immagine nella posizione corretta
            bitmap = ImageEditing.rotateBitmap(bitmap!!, orientation)


            selectphoto_imageview_register.setImageBitmap(bitmap)

            selectphoto_button_register.alpha = 0f

            //val bitmapDrawable=BitmapDrawable(bitmap)
            //imposto l'immagine selezionata
            //selectphoto_button_register.setBackgroundDrawable(bitmapDrawable)

        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            MY_PERMISSION_READ_EXTERNAL_STORAGE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //permessi ottenuti
                    Log.d("MainActivity", "apro la galleria")
                    val intent = Intent(Intent.ACTION_PICK)
                    intent.type = "image/*"
                    //apro selezionatore di immagini
                    startActivityForResult(intent, 0)
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }

    //ottengo la path dell'immagine nel dispositivo
    fun getPath(uri: Uri?): String? {
        @Suppress("DEPRECATION") val projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor =
            uri?.let { contentResolver.query(it, projection, null, null, null) } ?: return null
        @Suppress("DEPRECATION") val columnIndex =
            cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        val s = cursor.getString(columnIndex)
        cursor.close()
        return s
    }

    //completo la registrazione
    private fun performRegister() {
        val email = email_edittext_register.text.toString()
        val password = password_edittext_register.text.toString()

        if (email.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Perfavore, inserisci l'email o la password", Toast.LENGTH_SHORT)
                .show()
            return
        }

        //Autenticazione FireBase per creare un utente con username e password
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (!it.isSuccessful) return@addOnCompleteListener

                //altrimenti se riesce a registrarsi
                Log.d("Main", "utente creato con uid:" + it.result?.user?.uid)
                //carico l'immagine in Firebase
                uploadImageToFirebaseStorage()
            }
            //in caso di errore stampo un messaggio
            .addOnFailureListener {
                Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
            }
    }

    //carico l'immagine nello storage
    private fun uploadImageToFirebaseStorage() {
        var data: ByteArray? = null
        //carico immagine su firebase storage
        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")
        //se l'immagine non è stata caricata dall'utente ne scelgo una dalle risorse
        if (bitmap == null) {
            this.runOnUiThread {
                run {
                    try {
                        val stream = ByteArrayOutputStream()
                        var bitmap =
                            AppCompatResources.getDrawable(this, R.drawable.ic_user)!!.toBitmap()
                        bitmap = Bitmap.createScaledBitmap(bitmap, 100, 150, true)
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
                        data = stream.toByteArray()
                    } catch (e: IOException) {
                        println(e)
                    }
                }
            }
        } else {
            this.runOnUiThread {
                run {
                    try {
                        bitmap = Bitmap.createScaledBitmap(bitmap!!, 150, 150, true)
                        val baos = ByteArrayOutputStream()
                        bitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                        data = baos.toByteArray()
                    } catch (e: IOException) {
                        println(e)
                    }
                }
            }
        }

        ref.putBytes(data!!)
            //se l'immagine è stata caricata con successo
            .addOnSuccessListener {
                Log.d("RegisterActivity", "Immagine caricata con successo :${it.metadata?.path}")

                ref.downloadUrl.addOnSuccessListener {
                    it.toString()
                    Log.d("RegisterActivity", "file Location: " + it)
                    //salvo l'utente in Firebase
                    saveUserToFirebaseDatabase(it.toString())
                }
            }
            .addOnFailureListener {
                Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
            }
    }


    private fun saveUserToFirebaseDatabase(profileImageUrl: String) {
        //creo un oggetto User da insesire come figlio
        val uid = FirebaseAuth.getInstance().uid ?: ""
        val ref = FirebaseDatabase.getInstance().getReference("/users/$uid")
        val job = ""
        val bio = ""
        val place = ""
        val site = ""
        val ld = ""
        val twitter = ""
        val user = User(
            uid,
            username_edittext_register.text.toString(),
            profileImageUrl,
            job,
            bio,
            place,
            site,
            email_edittext_register.text.toString(),
            ld,
            twitter
        )
        ref.setValue(user)
            .addOnSuccessListener {
                Log.d("RegisterActivity", "Utente salvato nel database")
                //salvo l'utente anche nel DB locale
                saveUsertoLocalDB(
                    uid,
                    email_edittext_register.text.toString(),
                    username_edittext_register.text.toString(),
                    profileImageUrl
                )
            }
    }

    //metodo per salvare l'utente nel database locale
    fun saveUsertoLocalDB(id: String, email: String, username: String, photo: String) {
        val helper = DatabaseHelper(this)
        //se l'utente è stato inserito correttamente
        if (helper.insertUserSettings(id, email, username, photo) > 0) {
            //creo l'intent per la MainActivity
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }

    }

}



