package com.example.projectbook.service

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.example.projectbook.messages.ChatLog
import com.example.projectbook.models.ChatMessage
import com.example.projectbook.models.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.example.projectbook.R
import com.example.projectbook.views.UserItem

//serivce che resta in ascolto di nuovi messaggi
class MyIntentService : IntentService("MyIntentService") {

    override fun onHandleIntent(arg0: Intent?) {
    }


    var userItem: User? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        val myId = FirebaseAuth.getInstance().uid
        //riferimento agli ultimi messaggi ricevuti su Firebase
        val ref = FirebaseDatabase.getInstance().getReference("/latest-messages/$myId")
        ref.addChildEventListener(object : ChildEventListener {

            var usernamenotif: String? = null

            override fun onChildAdded(p0: DataSnapshot, p1: String?) {

            }

            //quando viene inserito un nuovo figlio
            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                //se il messaggio è indirizzato a me lo recupero
                if (p0.child("toId").getValue(String::class.java).equals(myId)) {
                    var user: User?
                    val chatMessage = p0.getValue(ChatMessage::class.java) ?: return
                    //recupero i dati del mittente del messaggio
                    val userref = p0.child("fromId").getValue(String::class.java) ?: return

                    val ref: DatabaseReference = FirebaseDatabase.getInstance().getReference("/users/$userref")

                    ref.addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onCancelled(p0: DatabaseError) {

                        }

                        override fun onDataChange(p0: DataSnapshot) {
                            user = p0.getValue(User::class.java)
                            userItem = user
                            //username del mittente
                            usernamenotif = user!!.username
                            //se è un immagine
                            if (chatMessage.type == "image") {
                                showNotification(usernamenotif!!, resources.getString(R.string.immagine))
                            } else {
                                showNotification(usernamenotif!!, chatMessage.text)
                            }
                        }
                    })


                }

            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
            }

            override fun onChildRemoved(p0: DataSnapshot) {
            }

            override fun onCancelled(p0: DatabaseError) {

            }
        })

        return Service.START_STICKY
    }


    fun showNotification(title: String, message: String) {
        val mNotificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                "notifications_id",
                "notifications",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            channel.description = "notifications"
            mNotificationManager.createNotificationChannel(channel)
        }
        val mBuilder = NotificationCompat.Builder(applicationContext, "YOUR_CHANNEL_ID")
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.pb))
        mBuilder.setSmallIcon(R.drawable.ic_notification)
            mBuilder.setColor(ContextCompat.getColor(this, R.color.chatLightGrey))
            .setSmallIcon(R.drawable.ic_notification) // notification icon
            .setContentTitle(title) // title for notification
            .setContentText(message)// message for notification
            .setAutoCancel(true) // clear notification after click

        try {
            val notification =
                RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val r = RingtoneManager.getRingtone(applicationContext, notification)
            r.play()
        } catch (e: Exception) {
            e.printStackTrace()
        }


        val intent = Intent(applicationContext, ChatLog::class.java)
        intent.putExtra("USER_KEY", userItem)
        val pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        mBuilder.setContentIntent(pi)
        mNotificationManager.notify(0, mBuilder.build())

    }


}

