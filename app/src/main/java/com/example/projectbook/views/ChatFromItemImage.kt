package com.example.projectbook.views

import android.content.Context
import android.content.Intent
import com.example.projectbook.R
import com.example.projectbook.models.User
import com.example.projectbook.profile.Profile
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.chat_from_row_image.view.*

//classe che definisce il layout dele immagini inviate
class ChatFromItemImage(val text: String, private val userId: String, private val time: String, val context: Context) :
    Item<GroupieViewHolder>() {
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        val targetImage = viewHolder.itemView.text_chat_from_row_image
        val myt: Target? = null
        targetImage.tag = myt
        Picasso.get().load(text).placeholder(R.drawable.progress_animation).into(targetImage)
        viewHolder.itemView.time_from_image.text = time

        var user: User?
        val ref2 = FirebaseDatabase.getInstance().getReference("/users/$userId")
        //listener per i dati
        ref2.addListenerForSingleValueEvent(object : ValueEventListener {

            override fun onDataChange(p0: DataSnapshot) {
                //salvo i dati nella variabile
                user = p0.getValue(User::class.java)
                val url = user?.profileImageUrl
                val target = viewHolder.itemView.imageView_chat_from_row_image
                target.setOnClickListener {
                    val intent =
                        Intent(context, Profile::class.java).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    intent.putExtra("USER_KEY", user)
                    context.startActivity(intent)
                }
                Picasso.get().load(url).placeholder(R.drawable.progress_animation).into(target)
            }

            override fun onCancelled(p0: DatabaseError) {

            }
        })
    }

    override fun getLayout(): Int {
        return R.layout.chat_from_row_image
    }

}