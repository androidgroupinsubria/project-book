package com.example.projectbook.views

import com.example.projectbook.R
import com.example.projectbook.groups.GroupFragment
import com.example.projectbook.models.ChatMessage
import com.example.projectbook.models.Group
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.latest_message_row.view.*

class GroupLatestMessageRow(private val chatMessage : ChatMessage) : Item<GroupieViewHolder>() {
    var group: Group? = null

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        if(chatMessage.type.equals("image")){
            viewHolder.itemView.message_textview_latest_message.text = GroupFragment.getContext()!!.resources.getString(R.string.immagine)
        } else {
            viewHolder.itemView.message_textview_latest_message.text = chatMessage.text
        }

        val groupId = chatMessage.toId
        val ref : DatabaseReference = FirebaseDatabase.getInstance().getReference("/groups/$groupId")
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                group = p0.getValue(Group::class.java)
                viewHolder.itemView.username_textview_latest_message.text = group?.title
                val targetImageView = viewHolder.itemView.imageview_latest_message
                Picasso.get().load(group?.groupImage).into(targetImageView)
            }
            override fun onCancelled(p0: DatabaseError) {
            }
        })
    }

    override fun getLayout(): Int {
        return R.layout.latest_message_row
    }
}