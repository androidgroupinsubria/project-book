package com.example.projectbook.views

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.text.format.DateFormat
import com.example.projectbook.R
import com.example.projectbook.models.Project
import com.example.projectbook.models.User
import com.example.projectbook.profile.Profile
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.project_layout.view.*
import java.util.*

class ProjectRow(val project: Project, val key: String) : Item<GroupieViewHolder>() {
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.title_textview.text = project.title
        viewHolder.itemView.desc_textview.text = project.desc

        var user2: User?
        val ref2 = FirebaseDatabase.getInstance().getReference("/users/" + project.authorId)

        ref2.addListenerForSingleValueEvent(object : ValueEventListener {

            @SuppressLint("SetTextI18n")
            override fun onDataChange(p0: DataSnapshot) {
                user2 = p0.getValue(User::class.java)
                viewHolder.itemView.creator_textview.text = "Autore: " + user2!!.username

                // set on-click listener
                viewHolder.itemView.creator_textview.setOnClickListener {
                    // your code to perform when the user clicks on the button
                    val context: Context
                    context = viewHolder.itemView.creator_textview.context
                    val intent = Intent(context, Profile::class.java)
                    intent.putExtra("USER_KEY", user2)
                    context.startActivity(intent)
                }
            }

            override fun onCancelled(p0: DatabaseError) {

            }
        })
        viewHolder.itemView.time_textview.text = getDate(project.timeStamp)
        Picasso.get().load(project.imageproject).placeholder(R.drawable.progress_animation)
            .into(viewHolder.itemView.image_view)
    }

    override fun getLayout(): Int {
        return R.layout.project_layout
    }

    fun getDate(timestamp: Long): String {
        val calendar = Calendar.getInstance(Locale.ENGLISH)
        calendar.timeInMillis = timestamp * 1000L
        val date = DateFormat.format("dd-MM-yyyy", calendar).toString()
        return date
    }


}