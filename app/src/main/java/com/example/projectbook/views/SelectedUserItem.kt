package com.example.projectbook.views

import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.projectbook.R
import com.example.projectbook.groups.AddGroupUsers
import com.example.projectbook.groups.NewGroup
import com.example.projectbook.models.User
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.selected_user.view.*

class SelectedUserItem(
    val user: User,
    private val usersAddedRecyclerview: RecyclerView,
    val newGroup: Boolean
) : Item<GroupieViewHolder>() {
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.setOnClickListener {
            if(this.newGroup){
                NewGroup.removeUserToList(user)
                NewGroup.removeItem(getItem(getPosition(this)), usersAddedRecyclerview)

                if (NewGroup.getAdapterCount() == 0) {
                    usersAddedRecyclerview.visibility = View.GONE
                }
                Toast.makeText(NewGroup.getContext(), user.username + " rimosso", Toast.LENGTH_SHORT)
                    .show()
            } else{
                AddGroupUsers.removeUserToList(user)
                AddGroupUsers.removeItem(getItem(getPosition(this)), usersAddedRecyclerview)

                if (AddGroupUsers.getAdapterCount() == 0) {
                    usersAddedRecyclerview.visibility = View.GONE
                }
                Toast.makeText(AddGroupUsers.getContext(), user.username + " aggiunto", Toast.LENGTH_SHORT)
                    .show()
            }
            usersAddedRecyclerview.scrollToPosition(0)
        }
        viewHolder.itemView.name_selected_user.text = user.username
        Picasso.get().load(user.profileImageUrl).placeholder(R.drawable.progress_animation)
            .into(viewHolder.itemView.image_selected_user)
    }

    override fun getLayout(): Int {
        return R.layout.selected_user
    }
}