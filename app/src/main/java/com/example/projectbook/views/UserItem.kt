package com.example.projectbook.views


import android.view.View
import com.example.projectbook.R
import com.example.projectbook.groups.GroupSettings
import com.example.projectbook.models.User
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.user_row_new_message.view.*

class UserItem(val userId: String, val deletable: Boolean) : Item<GroupieViewHolder>() {
    var user: User? = null

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        val ref = FirebaseDatabase.getInstance().getReference("/users/$userId")
        val position = getItem(getPosition(this))
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                user = p0.getValue(User::class.java)
                if (user != null) {
                    viewHolder.itemView.username.text = user?.username
                    Picasso.get().load(user?.profileImageUrl).placeholder(R.drawable.progress_animation)
                        .into(viewHolder.itemView.image)
                    if(deletable){
                        viewHolder.itemView.delete_button.visibility = View.VISIBLE
                        viewHolder.itemView.delete_button.setOnClickListener {
                            GroupSettings.deleteItem(position, user!!)
                        }
                    }
                }
            }

            override fun onCancelled(p0: DatabaseError) {

            }
        })
    }

    override fun getLayout(): Int {
        return R.layout.user_row_new_message
    }
}
