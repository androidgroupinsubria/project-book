package com.example.projectbook.views

import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.projectbook.R
import com.example.projectbook.groups.AddGroupUsers
import com.example.projectbook.groups.NewGroup
import com.example.projectbook.models.User
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.user_row_new_message.view.*

class UserItemGroup(
    val user: User,
    private val usersAddedRecyclerview: RecyclerView,
    val newGroup: Boolean
) : Item<GroupieViewHolder>() {
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.setOnClickListener {
            if (usersAddedRecyclerview.visibility == View.GONE) {
                usersAddedRecyclerview.visibility = View.VISIBLE
            }
            if(this.newGroup){
                NewGroup.addUserToList(user)
                NewGroup.addItem(getItem(getPosition(this)), usersAddedRecyclerview)
                Toast.makeText(NewGroup.getContext(), user.username + " aggiunto", Toast.LENGTH_SHORT)
                    .show()
            } else{
                AddGroupUsers.addUserToList(user)
                AddGroupUsers.addItem(getItem(getPosition(this)), usersAddedRecyclerview)
                Toast.makeText(AddGroupUsers.getContext(), user.username + " aggiunto", Toast.LENGTH_SHORT)
                    .show()
            }
            usersAddedRecyclerview.scrollToPosition(0)
        }
        viewHolder.itemView.username.setText(user.username)
        Picasso.get().load(user.profileImageUrl).placeholder(R.drawable.progress_animation)
            .into(viewHolder.itemView.image)
    }

    override fun getLayout(): Int {
        return R.layout.user_row_new_message
    }
}